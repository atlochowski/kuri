/*
 * Copyright (C) 2021 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.5
import QtQuick.Layouts 1.1

import Sailfish.Silica 1.0

Item {
    id: root

    property bool condensed: true

    property string label: ""
    property string value: ""
    property string unit: ""

    height: labelDisplay.height + valueDisplay.height + 2 * grid.columnSpacing

    /* the grid has the following layout
    +--------------------------------+
    |              label             |
    +--------------------------------+
    | spacer | value | unit | spacer |
    +--------------------------------+
    */

    GridLayout {
        id: grid
        anchors.fill: parent
        columns: 4
        rowSpacing: 0
        columnSpacing: Theme.paddingSmall

        Item {
            id: labelItem
            Layout.columnSpan: 4
            height: labelDisplay.height
            Layout.fillWidth: true

            Text {
                id: labelDisplay
                anchors.centerIn: parent

                font.pixelSize: root.condensed ? Theme.fontSizeExtraSmall : Theme.fontSizeSmall
                color: root.condensed ? Theme.secondaryHighlightColor : Theme.highlightColor

                text: root.label
            }
        }

        //horizontal spacer
        Item {
            Layout.fillWidth: true
            height: 1
        }

        Text {
            id: valueDisplay
            Layout.alignment: Qt.AlignBaseline | Qt.AlignRight

            font.pixelSize: root.condensed ? Theme.fontSizeMedium : Theme.fontSizeLarge
            color: Theme.primaryColor

            text: root.value
        }

        Text {
            id: unitDisplay
            Layout.alignment: Qt.AlignBaseline | Qt.AlignLeft

            font.pixelSize: root.condensed ? Theme.fontSizeExtraSmall : Theme.fontSizeSmall
            color: Theme.secondaryColor

            text: root.unit
        }

        //horizontal spacer
        Item {
            Layout.fillWidth: true
            height: 1
        }
    }
}
