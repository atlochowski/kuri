/*
 * Copyright (C) 2020 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQuick.Layouts 1.0

import Sailfish.Silica 1.0

import "../fields"
import "../tools"

import "../tools/JSTools.js" as JSTools

Rectangle {
    id: root
    anchors.fill: parent
    visible: false

    property color unlockButtonColor: "white"
    property color unlockButtonHighlightColor: "blue"

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        property bool triggerUnlock: false
        property bool inhibitAnimations: false
        property int moveStartPosition: 0
        readonly property int maxDelta: root.width / 6

        onPressed: {
            showUnlockButtonTimer.running = true;
            mouseArea.moveStartPosition = mouse.x;
            mouseArea.inhibitAnimations = true;
        }
        onReleased: {
            if (mouseArea.triggerUnlock) {
                root.visible = false;
            } else {
                showUnlockButtonTimer.restart();
            }
            mouseArea.inhibitAnimations = false;
            unlockLeftText.x = 0;
            unlockRightText.x = 0;
            lockScreenDisplay.opacity = 1;
        }

        onMouseXChanged: {
            var currentDelta = mouse.x - mouseArea.moveStartPosition;

            if (Math.abs(currentDelta) > mouseArea.maxDelta) {
                currentDelta = currentDelta > 0 ? mouseArea.maxDelta : -mouseArea.maxDelta;
                mouseArea.triggerUnlock = true;
            } else {
                mouseArea.triggerUnlock = false;
            }

            unlockLeftText.x = currentDelta;
            unlockRightText.x = currentDelta;
            lockScreenDisplay.opacity = 1 - Math.abs(currentDelta) / mouseArea.maxDelta;
        }
    }

    Timer {
        id: showUnlockButtonTimer
        interval: 2000
        running: false
        repeat: false
    }

    states: [
        State {
            when: !mouseArea.pressed && !showUnlockButtonTimer.running
            AnchorChanges { target: unlockLeft; anchors.left: undefined; anchors.right: parent.left }
            AnchorChanges { target: unlockRight; anchors.right: undefined; anchors.left: parent.right }
        },
        State {
            when: mouseArea.pressed || showUnlockButtonTimer.running
            AnchorChanges { target: unlockLeft; anchors.right: undefined; anchors.left: parent.left }
            AnchorChanges { target: unlockRight; anchors.left: undefined; anchors.right: parent.right }
        }
    ]

    transitions: Transition {
        AnchorAnimation { duration: 400 }
    }

    Item {
        id: unlockLeft
        anchors.right: parent.left
        height: parent.height
        width: unlockLeftText.width
        z: 1

        Text {
            id: unlockLeftText
            anchors.verticalCenter: parent.verticalCenter

            font.pixelSize: Theme.fontSizeHuge * 2
            font.bold: true
            text: " 〉"
            color: mouseArea.triggerUnlock ? unlockButtonHighlightColor : unlockButtonColor

            Behavior on x {
                enabled: !mouseArea.inhibitAnimations
                NumberAnimation { duration: 250 }
            }
        }
    }

    Item {
        id: unlockRight
        anchors.right: parent.left
        height: parent.height
        width: unlockRightText.width
        z: 1

        Text {
            id: unlockRightText
            anchors.verticalCenter: parent.verticalCenter

            font.pixelSize: Theme.fontSizeHuge * 2
            font.bold: true
            text: "〈 "
            color: mouseArea.triggerUnlock ? unlockButtonHighlightColor : unlockButtonColor

            Behavior on x {
                enabled: !mouseArea.inhibitAnimations
                NumberAnimation { duration: 250 }
            }
        }
    }

    ColumnLayout {
        spacing: 0
        anchors.leftMargin: Theme.paddingMedium
        anchors.rightMargin: Theme.paddingMedium
        anchors.bottomMargin: Theme.paddingMedium
        anchors.fill: parent
        z: 0

        Item {
            id: lockScreenDisplay
            Layout.fillWidth: true
            Layout.fillHeight: true

            readonly property var yTextMax: root.height - 3 * lockScreenDuration.height

            Behavior on opacity {
                enabled: !mouseArea.inhibitAnimations
                NumberAnimation { duration: 250 }
            }

            Timer {
                interval: 30000
                running: root.visible
                repeat: true
                triggeredOnStart: true

                onTriggered: lockScreenText.y = JSTools.fncGetRandomInt(0, lockScreenDisplay.yTextMax)
            }

            ColumnLayout {
                id: lockScreenText
                spacing: 0
                width: parent.width

                RecordPageField_Duration {
                    id: lockScreenDuration
                    width: parent.width
                    height: root.height / 5
                }
                RecordPageField_Distance {
                    width: parent.width
                    height: root.height / 5
                }
                RecordPageField_Pace {
                    width: parent.width
                    height: root.height / 5
                }
            }
        }
    }
}
