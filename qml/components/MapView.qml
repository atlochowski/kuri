/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.5
import Sailfish.Silica 1.0
import QtPositioning 5.2
import QtQuick.Layouts 1.1
import MapboxMap 1.0
import harbour.kuri 1.0
import "../tools/JSTools.js" as JSTools
import "../tools/SportsTracker.js" as ST
import "../tools/SharedResources.js" as SharedResources

// TODO: merge with Map.qml

Item
{
    id: root

    property var vTrackLinePoints

    property bool bHeartrateSupported: false
    property bool bPaceRelevantForWorkoutType: true

    //Map buttons
    property bool showSettingsButton: true
    property bool showMinMaxButton: true
    property bool showCenterButton: true

    property string sCurrentPosition: ""
    property var currentPosition: QtPositioning.coordinate(0, 0)

    property int iCurrentWorkout: 0

    readonly property bool maximized: minmaxButton.checked

    property alias mapStyle: map.styleUrl
    property bool measureSystemIsMetric: true

    function updateTrack()
    {
        var iPausePositionsIndex = 0;
        var trackPointsTemporary = [];

        map.styleUrl = Settings.mapStyle;


        //Go through array with track data points
        var trackPointCount = trackLoader.trackPointCount();
        for (var i=0; i<trackPointCount; i++)
        {
            //add this track point to temporary array. This will be used for drawing the track line
            trackPointsTemporary.push(trackLoader.trackPointAt(i));

            //Check if we have the first data point.
            if (i===0)
            {
                //This is the first data point, draw the start icon
                map.addSourcePoint("pointStartImage", trackLoader.trackPointAt(i));
                map.addImagePath("imageStartImage", Qt.resolvedUrl("../icons/map/circled-play-green-stroke.png"));
                map.addLayer("layerStartLayer", {"type": "symbol", "source": "pointStartImage"});
                map.setLayoutProperty("layerStartLayer", "icon-image", "imageStartImage");
                map.setLayoutProperty("layerStartLayer", "icon-size", 0.33);
                map.setLayoutProperty("layerStartLayer", "icon-allow-overlap", true);

                //Draw the current position icon to the first position
                sCurrentPosition = "currentPosition";
                map.addSourcePoint(sCurrentPosition,  trackLoader.trackPointAt(i));
                map.addImagePath("imageCurrentImage", Qt.resolvedUrl("../icons/map/hunt-violet-stroke.png"));
                map.addLayer("layerCurrentLayer", {"type": "symbol", "source": sCurrentPosition});
                map.setLayoutProperty("layerCurrentLayer", "icon-image", "imageCurrentImage");
                map.setLayoutProperty("layerCurrentLayer", "icon-size", 0.33);
                map.setLayoutProperty("layerCurrentLayer", "icon-allow-overlap", true);
            }

            //Check if we have the last data point, draw the stop icon
            if (i===(trackPointCount - 1))
            {
                map.addSourcePoint("pointEndImage",  trackLoader.trackPointAt(i));
                map.addImagePath("imageEndImage", Qt.resolvedUrl("../icons/map/stop-circled-red-stroke.png"));
                map.addLayer("layerEndLayer", {"type": "symbol", "source": "pointEndImage"});
                map.setLayoutProperty("layerEndLayer", "icon-image", "imageEndImage");
                map.setLayoutProperty("layerEndLayer", "icon-size", 0.33);
                map.setLayoutProperty("layerEndLayer", "icon-allow-overlap", true);

                //We have to create a track line here.
                map.addSourceLine("lineEndTrack", trackPointsTemporary)
                map.addLayer("layerEndTrack", { "type": "line", "source": "lineEndTrack" })
                map.setLayoutProperty("layerEndTrack", "line-join", "round");
                map.setLayoutProperty("layerEndTrack", "line-cap", "round");
                map.setPaintProperty("layerEndTrack", "line-color", "red");
                map.setPaintProperty("layerEndTrack", "line-width", 2.0);

                vTrackLinePoints = trackPointsTemporary;
                map.fitView(trackPointsTemporary);
            }

            //now check if we have a point where a pause starts
            if (JSTools.trackPausePointsTemporary.length > 0 && i===JSTools.trackPausePointsTemporary[iPausePositionsIndex])
            {
                //So this is a track point where a pause starts. The next one is the pause end!
                //Draw the pause start icon
                map.addSourcePoint("pointPauseStartImage" + iPausePositionsIndex.toString(), trackLoader.trackPointAt(i));
                map.addImagePath("imagePauseStartImage" + iPausePositionsIndex.toString(), Qt.resolvedUrl("../icons/map/pause-button-yellow-stroke.png"));
                map.addLayer("layerPauseStartLayer" + iPausePositionsIndex.toString(), {"type": "symbol", "source": "pointPauseStartImage" + iPausePositionsIndex.toString()});
                map.setLayoutProperty("layerPauseStartLayer" + iPausePositionsIndex.toString(), "icon-image", "imagePauseStartImage" + iPausePositionsIndex.toString());
                map.setLayoutProperty("layerPauseStartLayer" + iPausePositionsIndex.toString(), "icon-size", 0.33);
                map.setLayoutProperty("layerPauseStartLayer" + iPausePositionsIndex.toString(), "icon-allow-overlap", true);

                //Draw the pause end icon
                map.addSourcePoint("pointPauseEndImage" + iPausePositionsIndex.toString(),  trackLoader.trackPointAt(i+1));
                map.addImagePath("imagePauseEndImage" + iPausePositionsIndex.toString(), Qt.resolvedUrl("../icons/map/circled-play-yellow-stroke.png"));
                map.addLayer("layerPauseEndLayer" + iPausePositionsIndex.toString(), {"type": "symbol", "source": "pointPauseEndImage" + iPausePositionsIndex.toString()});
                map.setLayoutProperty("layerPauseEndLayer" + iPausePositionsIndex.toString(), "icon-image", "imagePauseEndImage" + iPausePositionsIndex.toString());
                map.setLayoutProperty("layerPauseEndLayer" + iPausePositionsIndex.toString(), "icon-size", 0.33);
                map.setLayoutProperty("layerPauseEndLayer" + iPausePositionsIndex.toString(), "icon-allow-overlap", true);


                //We can now create the track from start or end of last pause to start of this pause
                map.addSourceLine("lineTrack" + iPausePositionsIndex.toString(), trackPointsTemporary)
                map.addLayer("layerTrack" + iPausePositionsIndex.toString(), { "type": "line", "source": "lineTrack" + iPausePositionsIndex.toString() })
                map.setLayoutProperty("layerTrack" + iPausePositionsIndex.toString(), "line-join", "round");
                map.setLayoutProperty("layerTrack" + iPausePositionsIndex.toString(), "line-cap", "round");
                map.setPaintProperty("layerTrack" + iPausePositionsIndex.toString(), "line-color", "red");
                map.setPaintProperty("layerTrack" + iPausePositionsIndex.toString(), "line-width", 2.0);

                trackPointsTemporary = [];

                //set indexer to next pause position. But only if there is a further pause.
                if ((iPausePositionsIndex + 1) < JSTools.trackPausePointsTemporary.length)
                    iPausePositionsIndex++;
            }
        }

        map.updateSourcePoint(sCurrentPosition, trackLoader.trackPointAt(0));
    }

    onCurrentPositionChanged: {
        if (!root.currentPosition.isValid) {
            return;
        }
        map.updateSourcePoint(sCurrentPosition, root.currentPosition);
    }

    BusyIndicator
    {
        visible: true
        anchors.centerIn: root
        running: false
        size: BusyIndicatorSize.Large
    }

    ColumnLayout {
        anchors.fill: parent

        MapboxMap
        {
            id: map
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true
            Layout.fillHeight: true

            margins: Qt.rect(0.07, 0.07, 0.86, 0.86)
            center: QtPositioning.coordinate(52.51248, 13.36027)
            zoomLevel: 16.0
            minimumZoomLevel: 0
            maximumZoomLevel: 20
            pixelRatio: Theme.pixelRatio * scale
            readonly property real scale: 1.75
            accessToken: "pk.eyJ1IjoiamRyZXNjaGVyIiwiYSI6ImNqYmVta256YTJsdjUzMm1yOXU0cmxibGoifQ.JiMiONJkWdr0mVIjajIFZQ"
            cacheDatabaseMaximalSize: (Settings.mapCache)*1024*1024
            cacheDatabaseDefaultPath: true

            styleUrl: Settings.mapStyle

            Rectangle
            {
                id: centerButton
                anchors.left: parent.left
                anchors.leftMargin: Theme.paddingSmall
                anchors.top: parent.top
                anchors.topMargin: Theme.paddingSmall
                width: parent.width / 10
                height: parent.width / 10
                radius: width / 10
                color: "transparent"
                border.color: "#333333"
                border.width: 3
                visible: showCenterButton
                z: 200

                Rectangle
                {
                    anchors.fill: parent
                    radius: parent.radius
                    color: "#DDDDDD"
                    opacity: 0.75
                }
                MouseArea
                {
                    anchors.fill: parent
                    onReleased:
                    {
                        console.log("centerButton pressed");
                        map.fitView(vTrackLinePoints);
                    }
                }
                Image
                {
                    anchors.fill: parent
                    source: "../icons/map/hunt-pebble-padded.png"
                }
            }
            Rectangle
            {
                id: minmaxButton
                anchors.right: parent.right
                anchors.rightMargin: Theme.paddingSmall
                anchors.top: parent.top
                anchors.topMargin: Theme.paddingSmall
                width: parent.width / 10
                height: parent.width / 10
                radius: width / 10
                color: "transparent"
                border.color: "#333333"
                border.width: 3
                visible: showMinMaxButton
                z: 200

                property bool checked: false

                Rectangle
                {
                    anchors.fill: parent
                    radius: parent.radius
                    color: "#DDDDDD"
                    opacity: 0.75
                }
                MouseArea
                {
                    anchors.fill: parent
                    onReleased:
                    {
                        console.log("minmaxButton pressed");
                        minmaxButton.checked = !minmaxButton.checked
                    }
                }
                Image
                {
                    anchors.fill: parent
                    source: maximized ? "../icons/map/collapse-pebble-padded.png" : "../icons/map/expand-pebble-padded.png"
                }
            }
            Rectangle
            {
                id: settingsButton
                anchors.right: parent.right
                anchors.rightMargin: Theme.paddingSmall
                anchors.bottom: parent.bottom
                anchors.bottomMargin: Theme.paddingSmall
                width: parent.width / 10
                height: parent.width / 10
                radius: width / 10
                color: "transparent"
                border.color: "#333333"
                border.width: 3
                visible: showSettingsButton
                z: 200

                Rectangle
                {
                    anchors.fill: parent
                    radius: parent.radius
                    color: "#DDDDDD"
                    opacity: 0.75
                }
                MouseArea
                {
                    anchors.fill: parent
                    onReleased:
                    {
                        console.log("settingsButton pressed");
                        pageStack.push(Qt.resolvedUrl("../pages/MapSettingsPage.qml"));
                    }
                }
                Image
                {
                    anchors.fill: parent
                    source: "../icons/map/settings-pebble-padded.png"
                }
            }

            MapboxMapGestureArea
            {
                id: mouseArea
                enabled: root.maximized
                map: map
                anchors.fill: parent
                activeClickedGeo: true
                activeDoubleClickedGeo: true
                activePressAndHoldGeo: false
                z: 100

                /*
                onReleased:
                {
                    console.log("onReleased: " + mouse);
                }

                onPressAndHold:
                {
                    console.log("onPressAndHold: " + mouse);
                }

                onPressAndHoldGeo:
                {
                    console.log("onPressAndHoldGeo: " + mouse);
                }
                */

                onDoubleClicked:
                {
                    //console.log("onDoubleClicked: " + mouse)
                    map.setZoomLevel(map.zoomLevel + 1, Qt.point(mouse.x, mouse.y) );
                }
                onDoubleClickedGeo:
                {
                    //console.log("onDoubleClickedGeo: " + geocoordinate);
                    map.center = geocoordinate;
                }
            }

            Item
            {
                id: scaleBar
                anchors.bottom: parent.bottom
                anchors.bottomMargin: Theme.paddingLarge
                anchors.left: parent.left
                anchors.leftMargin: Theme.paddingLarge
                height: base.height + text.height + text.anchors.bottomMargin
                opacity: 0.9
                visible: scaleWidth > 0
                z: 100

                property real   scaleWidth: 0
                property string text: ""

                Rectangle {
                    id: base
                    anchors.bottom: scaleBar.bottom
                    color: "#333333"
                    height: Math.floor(Theme.pixelRatio * 3)
                    width: scaleBar.scaleWidth
                }

                Rectangle {
                    anchors.bottom: base.top
                    anchors.left: base.left
                    color: "#333333"
                    height: Math.floor(Theme.pixelRatio * 10)
                    width: Math.floor(Theme.pixelRatio * 3)
                }

                Rectangle {
                    anchors.bottom: base.top
                    anchors.right: base.right
                    color: "#333333"
                    height: Math.floor(Theme.pixelRatio * 10)
                    width: Math.floor(Theme.pixelRatio * 3)
                }

                Text {
                    id: text
                    anchors.bottom: base.top
                    anchors.bottomMargin: Math.floor(Theme.pixelRatio * 4)
                    anchors.horizontalCenter: base.horizontalCenter
                    color: "black"
                    font.bold: true
                    font.family: "sans-serif"
                    font.pixelSize: Math.round(Theme.pixelRatio * 18)
                    horizontalAlignment: Text.AlignHCenter
                    text: scaleBar.text
                }

                function siground(x, n) {
                    // Round x to n significant digits.
                    var mult = Math.pow(10, n - Math.floor(Math.log(x) / Math.LN10) - 1);
                    return Math.round(x * mult) / mult;
                }

                function roundedDistace(dist)
                {
                    // Return dist rounded to an even amount of user-visible units,
                    // but keeping the value as meters.

                    if (measureSystemIsMetric)
                    {
                        return siground(dist, 1);
                    }
                    else
                    {
                        return dist >= 1609.34 ?
                            siground(dist / 1609.34, 1) * 1609.34 :
                            siground(dist * 3.28084, 1) / 3.28084;
                    }
                }

                function update()
                {
                    // Update scalebar for current zoom level and latitude.

                    var meters = map.metersPerPixel * map.width / 4;
                    var dist = scaleBar.roundedDistace(meters);

                    scaleBar.scaleWidth = dist / map.metersPerPixel

                    console.log("dist: " + dist);

                    var sUnit = "";
                    var iDistance = 0;

                    if (measureSystemIsMetric)
                    {
                        sUnit = "m";
                        iDistance = Math.ceil(dist);
                        if (dist >= 1000)
                        {
                            sUnit = "km";
                            iDistance = dist / 1000.0;
                            iDistance = Math.ceil(iDistance);
                        }
                    }
                    else
                    {
                        dist = dist * 3.28084;  //convert to feet

                        sUnit = "ft";
                        iDistance = Math.ceil(dist);
                        if (dist >= 5280)
                        {
                            sUnit = "mi";
                            iDistance = dist / 5280.0;
                            iDistance = Math.ceil(iDistance);
                        }
                    }

                    scaleBar.text = iDistance.toString() + " " + sUnit
                }

                Connections
                {
                    target: map
                    onMetersPerPixelChanged: scaleBar.update()
                    onWidthChanged: scaleBar.update()
                }
            }
        }
    }
}
