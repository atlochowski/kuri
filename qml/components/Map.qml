/*
 * Copyright (C) 2019 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.0
import QtQml 2.2
import QtQuick.Layouts 1.0

import Sailfish.Silica 1.0
import QtPositioning 5.2
import MapboxMap 1.0

import harbour.kuri 1.0

Item {
    id: root

    property bool showSettingsButton: true
    property bool showMinMaxButton: true
    property bool showCenterButton: true
    property bool mapGesturesEnabled: true

    property var currentPosition: QtPositioning.coordinate(52.51248, 13.36027)

    readonly property bool maximized: minmaxButton.checked

    property alias mapStyle: map.styleUrl

    signal showSettings()

    // TODO merge MapViewPage.qml with this one
    // - Mode enum with `Tracking` and `Viewing`
    //   - access the track recorder and track loader directly
    //   - some parts are only shown if `Mode.Tracking` and other only if `Mode.Viewing`
    // - PointAttribute enum with `None`, `Start`, `Stop`, `PauseBegin`, `PauseEnd`, `Section`
    //   - make TrackRecorder::newTrackPoint emit this attribute; maybe create a struct with QGeoCoordinate, index and this attribute
    //   - make addMapPoint use this attribute

    onCurrentPositionChanged: {
        //Map interaction is only done when map is really shown and application active
        if (!internal.viewActive) {
            return;
        }
        updateCurrentPosition()
    }

    function updateCurrentPosition()
    {
        if (!root.currentPosition.isValid) {
            return;
        }
        map.updateSourcePoint(internal.sources.currentPosition, root.currentPosition);
        updateMapUncertainty();

        if (Settings.mapMode === 0  && !root.maximized) {
            map.center = root.currentPosition;
        }
    }

    function updateMapUncertainty()
    {
        if (map.metersPerPixel > 0)
        {
            map.setPaintProperty(internal.layers.locationUncertainty, "circle-radius", (GeoPositionInfo.accuracy / map.metersPerPixel) / map.pixelRatio);
        }
    }

    function addMapPoint(coordinate, iPointIndex)
    {
        if (!internal.viewActive) {
            internal.trackLinePointCache.push(coordinate);
            internal.trackLinePointIndexCache.push(iPointIndex);
            return;
        }

        internal.setCachedMapPoints();

        setMapPoint(coordinate, iPointIndex);
    }

    function setMapPoint(coordinate, iPointIndex)
    {
        var vLineArray = [];

        //Recognize the start of a workout
        if (iPointIndex === 0 && TrackRecorder.running && !TrackRecorder.isEmpty)
        {
            //This is the first data point, draw the start icon
            map.addSourcePoint(internal.sources.trackBegin,  coordinate);
            map.addImagePath("kuri-image-begin", Qt.resolvedUrl("../icons/map/circled-play-green-stroke.png"));
            const LayerTrackMarkerBegin = internal.layers.trackMarker + "-begin"
            map.addLayer(LayerTrackMarkerBegin, {"type": "symbol", "source": internal.sources.trackBegin}, internal.layers.trackMarker);
            map.setLayoutProperty(LayerTrackMarkerBegin, "icon-image", "kuri-image-begin");
            map.setLayoutProperty(LayerTrackMarkerBegin, "icon-size", 0.33);
            map.setLayoutProperty(LayerTrackMarkerBegin, "icon-allow-overlap", true);

            //Create temp line array
            vLineArray = [];
            //Write first coordinate to line array
            vLineArray.push(coordinate);
            //Save that to global array
            map.trackLinePoints = vLineArray;

            map.trackLineKey = "lineEndTrack";

            //We have to create a track line here.
            map.addSourceLine(map.trackLineKey, map.trackLinePoints)
            const LayerTrackBegin = internal.layers.track + "-" + map.trackIndex.toString();
            map.addLayer(LayerTrackBegin, { "type": "line", "source": map.trackLineKey }, internal.layers.track)
            map.setLayoutProperty(LayerTrackBegin, "line-join", "round");
            map.setLayoutProperty(LayerTrackBegin, "line-cap", "round");
            map.setPaintProperty(LayerTrackBegin, "line-color", "red");
            map.setPaintProperty(LayerTrackBegin, "line-width", 2.0);
        }

        //Recognize the start of a pause
        if (TrackRecorder.running && !TrackRecorder.isEmpty && iPointIndex > 0 && TrackRecorder.pausePointAt(iPointIndex - 1) === false && TrackRecorder.pausePointAt(iPointIndex) === true)
        {
            //Draw the pause start icon
            const SourcePauseBegin = internal.sources.trackPause + "-begin-" + map.trackIndex.toString();
            map.addSourcePoint(SourcePauseBegin,  coordinate);
            map.addImagePath("kuri-image-pause-begin" + map.trackIndex.toString(), Qt.resolvedUrl("../icons/map/pause-button-yellow-stroke.png"));
            const LayerPauseBegin = internal.layers.trackMarker + "-pause-begin-" + map.trackIndex.toString();
            map.addLayer(LayerPauseBegin, {"type": "symbol", "source": SourcePauseBegin}, internal.layers.trackMarker);
            map.setLayoutProperty(LayerPauseBegin, "icon-image", "kuri-image-pause-begin" + map.trackIndex.toString());
            map.setLayoutProperty(LayerPauseBegin, "icon-size", 0.33);
            map.setLayoutProperty(LayerPauseBegin, "icon-allow-overlap", true);

            //set indexer to next pause position.
            map.trackIndex++;
        }

        //Recognize the end of a pause
        if (TrackRecorder.running && !TrackRecorder.isEmpty && iPointIndex > 0 && TrackRecorder.pausePointAt(iPointIndex - 1) === true && TrackRecorder.pausePointAt(iPointIndex) === false)
        {
            //So this is a track point where a pause starts. The next one is the pause end!
            //Draw the pause end icon
            const SourcePauseEnd = internal.sources.trackPause + "-end-" + map.trackIndex.toString();
            map.addSourcePoint(SourcePauseEnd, coordinate);
            map.addImagePath("kuri-image-pause-end" + map.trackIndex.toString(), Qt.resolvedUrl("../icons/map/circled-play-yellow-stroke.png"));
            const LayerPauseEnd = internal.layers.trackMarker + "-pause-end-" + map.trackIndex.toString();
            map.addLayer(LayerPauseEnd, {"type": "symbol", "source": SourcePauseEnd}, internal.layers.trackMarker);
            map.setLayoutProperty(LayerPauseEnd, "icon-image", "kuri-image-pause-end" + map.trackIndex.toString());
            map.setLayoutProperty(LayerPauseEnd, "icon-size", 0.33);
            map.setLayoutProperty(LayerPauseEnd, "icon-allow-overlap", true);

            //Doing the update here is OK because there should not be too many pauses.
            map.updateSourceLine(map.trackLineKey, map.trackLinePoints);

            //Start new trackline here
            //Create fresh temp line array
            vLineArray = [];
            //Write first coordinate of new track segment to line array
            vLineArray.push(coordinate);
            //Save that to global array
            map.trackLinePoints = vLineArray;

            map.trackLineKey = "kuri-source-track-" + map.trackIndex.toString();

            //We have to create a track line here.
            map.addSourceLine(map.trackLineKey, map.trackLinePoints)
            const LayerTrack = internal.layers.track + "-" + map.trackIndex.toString();
            map.addLayer(LayerTrack, { "type": "line", "source": map.trackLineKey }, internal.layers.track)
            map.setLayoutProperty(LayerTrack, "line-join", "round");
            map.setLayoutProperty(LayerTrack, "line-cap", "round");
            map.setPaintProperty(LayerTrack, "line-color", "red");
            map.setPaintProperty(LayerTrack, "line-width", 2.0);
        }

        //If the current point is not the first one and not a pause point, add it to the current track
        if (TrackRecorder.running && !TrackRecorder.isEmpty && iPointIndex !== 0 && TrackRecorder.pausePointAt(iPointIndex) === false)
        {
            //Create temp line array and set current points array to it. Must use a JS array here because QML arrays don't allow for push!
            vLineArray = map.trackLinePoints;
            //Write first coordinate to line array
            vLineArray.push(coordinate);
            //Save that to global array
            map.trackLinePoints = vLineArray;

            if (Settings.mapMode === 1 && !root.maximized) //center track on map
                map.fitView(map.trackLinePoints);
        }
    }

    function updateTrack() {
        map.updateSourceLine(map.trackLineKey, map.trackLinePoints);
    }

    Item {
        id: internal

        readonly property bool viewActive: root.visible && appWindow.applicationActive

        readonly property var layers: QtObject {
            readonly property string location: "kuri-layer-location"
            readonly property string locationUncertainty: "kuri-layer-location-uncertainty"
            readonly property string track: "kuri-layer-track"
            readonly property string trackMarker: "kuri-layer-track-marker"
        }

        readonly property var sources: QtObject {
            readonly property string currentPosition: "kuri-source-current-position"
            readonly property string trackBegin: "kuri-source-track-begin"
            readonly property string trackSection: "kuri-source-track-section"
            readonly property string trackPause: "kuri-source-track-pause"
            readonly property string trackEnd: "kuri-source-track-end"
        }

        property var trackLinePointCache: []
        property var trackLinePointIndexCache: []

        function setCachedMapPoints() {
            if (internal.trackLinePointCache.length > 0) {
                for(var i = 0; i < internal.trackLinePointCache.length; i++) {
                    setMapPoint(internal.trackLinePointCache[i], internal.trackLinePointIndexCache[i]);
                }

                internal.trackLinePointCache = [];
                internal.trackLinePointIndexCache = [];
            }
        }

        onViewActiveChanged: {
            if (viewActive) {
                updateCurrentPosition();
                internal.setCachedMapPoints();
                root.updateTrack();
            }
        }
    }

    MapboxMap {
        id: map
        anchors.fill: parent

        center: QtPositioning.coordinate(52.51248, 13.36027)
        margins: Qt.rect(0.07, 0.07, 0.86, 0.86)
        zoomLevel: 16
        minimumZoomLevel: 0
        maximumZoomLevel: 20
        pixelRatio: Theme.pixelRatio * scale
        readonly property real scale: 1.75

        accessToken: "pk.eyJ1IjoiamRyZXNjaGVyIiwiYSI6ImNqYmVta256YTJsdjUzMm1yOXU0cmxibGoifQ.JiMiONJkWdr0mVIjajIFZQ"
        cacheDatabaseDefaultPath: true

        property string trackLineKey: ""
        property var trackLinePoints
        property int trackIndex: 0

        onMetersPerPixelChanged: updateMapUncertainty()

        Component.onCompleted: {
            map.styleUrl = Settings.mapStyle;
            cacheDatabaseMaximalSize = Settings.mapCache * 1024 * 1024;

            map.addSourcePoint("kuri-source-dummy", map.center);
            map.addLayer(internal.layers.trackMarker, {"type": "line", "source" : "kuri-source-dummy"});
            map.addLayer(internal.layers.track, {"type": "line", "source" : "kuri-source-dummy"});
            map.addLayer(internal.layers.location, {"type": "line", "source" : "kuri-source-dummy"});

            //Create current position point on map
            map.addSourcePoint(internal.sources.currentPosition, map.center);

            map.addLayer(internal.layers.locationUncertainty, {"type": "circle", "source": internal.sources.currentPosition}, internal.layers.location);
            map.setPaintProperty(internal.layers.locationUncertainty, "circle-radius", (300 / map.metersPerPixel) / map.pixelRatio);
            map.setPaintProperty(internal.layers.locationUncertainty, "circle-color", "#7f1515");
            map.setPaintProperty(internal.layers.locationUncertainty, "circle-opacity", 0.25);

            const LayerLocationRing = internal.layers.location + "-ring";
            map.addLayer(LayerLocationRing, {"type": "circle", "source": internal.sources.currentPosition}, internal.layers.location);
            map.setPaintProperty(LayerLocationRing, "circle-radius", 6.0);
            map.setPaintProperty(LayerLocationRing, "circle-color", "black");

            const LayerLocationFill = internal.layers.location + "-fill";
            map.addLayer(LayerLocationFill, {"type": "circle", "source": internal.sources.currentPosition}, internal.layers.location);
            map.setPaintProperty(LayerLocationFill, "circle-radius", 4.5);
            map.setPaintProperty(LayerLocationFill, "circle-color", "#a01b1b");
        }

        Rectangle
        {
            id: centerButton
            anchors.left: parent.left
            anchors.leftMargin: Theme.paddingSmall
            anchors.top: parent.top
            anchors.topMargin: Theme.paddingSmall
            width: parent.width / 10
            height: parent.width / 10
            radius: width / 10
            color: "transparent"
            border.color: "#333333"
            border.width: 3
            visible: showCenterButton
            z: 200

            Rectangle
            {
                anchors.fill: parent
                radius: parent.radius
                color: "#DDDDDD"
                opacity: 0.75
            }
            MouseArea
            {
                anchors.fill: parent
                onReleased: {
                    if(root.currentPosition.isValid) {
                        map.center = root.currentPosition;
                    } else {
                        console.log("Error: currentPosition is invalid!");
                    }
                }
            }
            Image
            {
                anchors.fill: parent
                source: "../icons/map/hunt-pebble-padded.png"
            }
        }
        Rectangle
        {
            id: minmaxButton
            anchors.right: parent.right
            anchors.rightMargin: Theme.paddingSmall
            anchors.top: parent.top
            anchors.topMargin: Theme.paddingSmall
            width: parent.width / 10
            height: parent.width / 10
            radius: width / 10
            color: "transparent"
            border.color: "#333333"
            border.width: 3
            visible: showMinMaxButton
            z: 200

            property bool checked: false

            Rectangle
            {
                anchors.fill: parent
                radius: parent.radius
                color: "#DDDDDD"
                opacity: 0.75
            }
            MouseArea
            {
                anchors.fill: parent
                onReleased: minmaxButton.checked = !minmaxButton.checked
            }
            Image
            {
                anchors.fill: parent
                source: maximized ? "../icons/map/collapse-pebble-padded.png" : "../icons/map/expand-pebble-padded.png"
            }
        }
        Rectangle
        {
            id: settingsButton
            anchors.right: parent.right
            anchors.rightMargin: Theme.paddingSmall
            anchors.bottom: parent.bottom
            anchors.bottomMargin: Theme.paddingSmall
            width: parent.width / 10
            height: parent.width / 10
            radius: width / 10
            color: "transparent"
            border.color: "#333333"
            border.width: 3
            visible: showSettingsButton
            z: 200

            Rectangle
            {
                anchors.fill: parent
                radius: parent.radius
                color: "#DDDDDD"
                opacity: 0.75
            }
            MouseArea
            {
                anchors.fill: parent
                onReleased:
                {
                    showSettings();
                }
            }
            Image
            {
                anchors.fill: parent
                source: "../icons/map/settings-pebble-padded.png"
            }
        }

        MapboxMapGestureArea
        {
            id: gestureArea
            enabled: mapGesturesEnabled
            map: map
            anchors.fill: parent
            activeClickedGeo: true
            activeDoubleClickedGeo: true
            activePressAndHoldGeo: false
            z: 100

            onDoubleClicked:
            {
                map.setZoomLevel(map.zoomLevel + 1, Qt.point(mouse.x, mouse.y) );
            }
            onDoubleClickedGeo:
            {
                map.center = geocoordinate;
            }
        }
    }
}
