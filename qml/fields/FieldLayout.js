/*
 * Copyright (C) 2017 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

.pragma library

var layout = [
    [{enabled: true, source: "RecordPageField_Duration.qml"}, {enabled: false, source: ""}],
    [{enabled: true, source: "RecordPageField_Distance.qml"}, {enabled: true, source: "RecordPageField_Pace.qml"}],
    [{enabled: true, source: "RecordPageField_Calories.qml"}, {enabled: true, source: "RecordPageField_HeartRate.qml"}],
    [{enabled: true, source: "RecordPageField_Duration_Section.qml"}, {enabled: true, source: "RecordPageField_Elevation.qml"}]
]
