/*
 * Copyright (C) 2017 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import QtQml 2.2
import QtQuick.Layouts 1.0

import Sailfish.Silica 1.0

import harbour.kuri 1.0

import "../tools"

Item {
    id: root

    property bool condensed: true
    
    property string label
    property string value
    property string unit


    /* the grid has the following layout
    +------------------------------+
    |            spacer            |
    +------------------------------+
    |            label             |
    +------------------------------+
    | spacer |    value   |        |
    +---------------------| spacer |
    |              unit   |        |
    +------------------------------+
    |            spacer            |
    +------------------------------+
    */

    GridLayout {
        anchors.fill: parent
        columns: 3
        rowSpacing: 0
        columnSpacing: root.condensed ? Theme.paddingSmall : 0

        //vertical spacer
        Item {
            Layout.fillHeight: true
            Layout.columnSpan: 3
        }

        Item {
            Layout.columnSpan: 3
            height: labelDisplay.height / 2
            Layout.fillWidth: true

            Text {
                id: labelDisplay
                anchors.verticalCenter: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                Layout.alignment: Qt.AlignBottom | Qt.AlignHCenter

                font.pixelSize: root.condensed ? Theme.fontSizeSmall : Theme.fontSizeMedium
                color: RecordPageColorTheme.colors.highlight

                text: root.label
            }
        }

        //horizontal spacer
        Item {
            Layout.fillWidth: true
        }

        Text {
            id: valueDisplay
            Layout.alignment: Qt.AlignCenter

            font.pixelSize: root.condensed ? Theme.fontSizeHuge * 1.75 : Theme.fontSizeHuge * 2
            color: RecordPageColorTheme.colors.primary

            text: root.value
        }

        //horizontal spacer
        Item {
            Layout.fillWidth: true
            Layout.rowSpan: 2
        }

        Item {
            Layout.columnSpan: 2
            height: unitDisplay.height / 2
            Layout.alignment: Qt.AlignTop |  Qt.AlignRight
            Text {
                id: unitDisplay
                anchors.verticalCenter: parent.top
                anchors.right: parent.left
                Layout.alignment: Qt.AlignVCenter | Qt.AlignRight


                font.pixelSize: root.condensed ? Theme.fontSizeSmall : Theme.fontSizeMedium
                color: RecordPageColorTheme.colors.secondary

                text: root.unit
            }
        }

        //vertical spacer
        Item {
            Layout.fillHeight: true
            Layout.columnSpan: 3
        }
    }
}
