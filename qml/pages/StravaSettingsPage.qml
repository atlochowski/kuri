/*
 * Copyright (C) 2017 Adam Pigg <adam@piggz.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.kuri 1.0

Page {
    id: page
    allowedOrientations: Orientation.Portrait

    property bool stravaLinked: stravaOAuth.linked
    property bool downloadingGPX: false
    property string userName: stravaLinked ? Settings.stravaUserName : qsTr("not logged in")
    property string country: stravaLinked ? Settings.stravaCountry : ""

    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: column.height + Theme.paddingLarge;
        VerticalScrollDecorator {}

        PullDownMenu
        {
            id: menu
            MenuItem
            {
                id: btnAuth
                text: stravaLinked ? qsTr("Logout") : qsTr("Login")
                onClicked: {
                    if (stravaLinked) {
                        stravaOAuth.unlink();
                    } else {
                        stravaOAuth.authorizeInBrowser();
                    }
                }
            }
        }

        Column
        {
            id: column
            width: page.width
            spacing: Theme.paddingLarge
            PageHeader
            {
                title: qsTr("Strava settings")
            }

            Column {
                width: parent.width
                spacing: Theme.paddingLarge

                Label{
                    id: lblUser
                    horizontalAlignment: Text.AlignHCenter
                    width: parent.width
                    text: qsTr("User Name: ") + userName
                }
                Label{
                    id: lblCountry
                    horizontalAlignment: Text.AlignHCenter
                    width: parent.width
                    text: qsTr("Country: ") + country
                }
            }
        }
    }
}
