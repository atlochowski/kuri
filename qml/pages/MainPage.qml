/*
 * Copyright (C) 2017 Jens Drescher, Germany
 * Copyright (C) 2021 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import QtQuick.Layouts 1.0

import Sailfish.Silica 1.0

import harbour.kuri 1.0

import "../tools/SharedResources.js" as SharedResources
import "../tools/Thresholds.js" as Thresholds
import "../tools/JSTools.js" as JSTools
import "../tools/SportsTracker.js" as ST

Page
{
    id: mainPage

    allowedOrientations: Orientation.Portrait

    property string sWorkoutDuration: ""
    property string sWorkoutDistance: ""
    property string sWorkoutFilter: ""

    property int iCurrentWorkout: 0

    TrackLoader
    {
        id: trackLoader
    }

    function fncSetWorkoutFilter()
    {
        var distanceKilometer = SharedResources.arrayLookupWorkoutFilterMainPageTableByName[Settings.workoutTypeMainPage].iDistance / 1000;
        var distance = (Settings.measureSystem === 0) ? distanceKilometer : JSTools.fncConvertDistanceToImperial(distanceKilometer);
        var decimals = 0;
        if (distance < 10)
        {
            decimals = 2;
        }
        else if (distance < 100)
        {
            decimals = 1;
        }
        sWorkoutDistance = distance.toFixed(decimals) + " " + (Settings.measureSystem === 0 ? qsTr("km") : qsTr("mi"));

        var durationInHours = SharedResources.arrayLookupWorkoutFilterMainPageTableByName[Settings.workoutTypeMainPage].iDuration / (60 * 60);
        //: the total hours for all workouts
        sWorkoutDuration = qsTr("%1 hrs").arg(durationInHours.toFixed(1));

        ActivityHistory.setFilterFixedString(Settings.workoutTypeMainPage === "allworkouts" ?  "" : Settings.workoutTypeMainPage);
    }

    function fncCheckAutosave()
    {

        console.log("TrackRecorder.isEmpty: " + TrackRecorder.isEmpty.toString());

        // TODO fix recovery
        TrackRecorder.clearTrack();

        if (TrackRecorder.isEmpty === false)
        {
            var dialog = pageStack.push(id_Dialog_Autosave)
            dialog.accepted.connect(function()
            {
                //console.log("Accepted");
                //Accept means the users wants to resume the workout
                TrackRecorder.workoutType = Settings.workoutType;
                pageStack.push(Qt.resolvedUrl("RecordPage.qml"));
            })
            dialog.rejected.connect(function()
            {
                console.log("Canceled");
                //Cancel means the users wants to delete the workout
                TrackRecorder.clearTrack();
            })
        }
    }

    function displayNotification(text, type, delay){
        console.log(text);
        load_text.text = text;
        if (type === "info"){
            ntimer.interval = delay;
            load_text.color = Theme.primaryColor;
        }
        else if (type === "success"){
            ntimer.interval = delay;
            load_text.color = Theme.primaryColor;
        }
        else if (type === "error"){
            if (ST.loginstate == 1 && ST.recycledlogin === true){
                console.log("Sessionkey might be too old. Trying to login again");
                Settings.stSessionkey = "";
                ST.SESSIONKEY = "";
                recycledlogin = false;
                ST.uploadToSportsTracker(stSharing, stComment, displayNotification);
            }
            else{
                ntimer.interval = delay;
                load_text.color = Theme.secondaryHighlightColor;
            }
        }
        ntimer.restart();
        ntimer.start();
    }

    Timer{
        id:ntimer;
        running: false;
        interval: 2000;
        repeat: false;
        onTriggered: {
            detail_busy.running = false;
            main_flickable.visible = true;
            load_text.visible = false;
            ntimer.restart();
            ntimer.start();
        }
    }


    Component.onCompleted:
    {
        console.log("First Active MainPage");

        //Read settings to QML variables
        var sTemp = Settings.hrmdevice;
        sTemp = sTemp.split(',');
        if (sTemp.length === 2)
        {
            sHRMAddress = sTemp[0];
            sHRMDeviceName = sTemp[1];
        }
        else
        {
            sHRMAddress = "";
            sHRMDeviceName = "";
        }

        //Search for pebble watch
        if (Settings.enablePebble)
        {
            var sPebbleList = PebbleManagerComm.getListWatches();
            console.log("sPebbleList: " + sPebbleList);

            if (sPebbleList !== undefined && sPebbleList.length > 0)
            {
                //There might be more than one pebble found
                if (sPebbleList.length > 1)
                {
                    //Now read the last used pebble string from settings
                    var sLastUsedPebbleString = Settings.pebbleIDstring;

                    console.log("sLastUsedPebbleString: " + sLastUsedPebbleString);

                    //Check if the last used pebble string is in the pebble list
                    for (var j = 0; j < sPebbleList.length; j++)
                    {
                        if (sLastUsedPebbleString === sPebbleList[j])
                        {
                            sPebblePath = sPebbleList[j];
                            break;
                        }
                    }
                }
                else
                {
                    //A pebble was found, we have now a DBus path to it.
                    //If there are more than one pebble, use the first one.
                    sPebblePath = sPebbleList[0];
                }

                //This sets the path with the BT address to the C++ class and inits the DBUS communication object
                if (sPebblePath !== "") PebbleWatchComm.setServicePath(sPebblePath);
            }
        }

        //Sport app on the pebble is no longer required
        bPebbleSportAppRequired = false;

        //If pebble is NOT connected, check if it's connected now
        if (sPebblePath !== "" && Settings.enablePebble && !bPebbleConnected)
        {
            bPebbleConnected = PebbleWatchComm.isConnected();
        }
    }

    onStatusChanged:
    {
        //This is loaded everytime the page is displayed
        if (status === PageStatus.Active)
        {
            console.log("Active MainPage");

            //stop positioning
            GeoPositionInfo.stopUpdates();

            //close pebble sport app
            if (sPebblePath !== "" && Settings.enablePebble)
            {
                pebbleComm.fncClosePebbleApp("4dab81a6-d2fc-458a-992c-7a1f3b96a970");
            }

            //Save the object of this page for back jumps
            vMainPageObject = pageStack.currentPage;
            console.log("vMainPageObject: " + vMainPageObject.toString());
        }
    }

    BusyIndicator
    {
        id: detail_busy
        visible: false
        anchors.centerIn: main_flickable
        running: true
        size: BusyIndicatorSize.Large
    }
    Label {
         id:load_text
         width: parent.width
         anchors.top: detail_busy.bottom
         anchors.topMargin: 25;
         horizontalAlignment: Label.AlignHCenter
         visible: false
         text: "loading..."
         font.pixelSize: Theme.fontSizeMedium
    }

    Connections
    {
        target: ActivityHistory
        onTrackLoadingFinished:     //This is called from C++ if the loading of the GPX files is ready
        {
            // reset distance and duration in arrayLookupWorkoutFilterMainPageTableByName
            for (var type in SharedResources.arrayLookupWorkoutFilterMainPageTableByName){
                SharedResources.arrayLookupWorkoutFilterMainPageTableByName[type].iDistance = 0;
                SharedResources.arrayLookupWorkoutFilterMainPageTableByName[type].iDuration = 0;
            }

            var sWorkoutCurrent, fDistanceCurrent, iDurationCurrent
            //Go through all workouts
            for (var i = 0; i < ActivityHistory.rowCount(); i++)
            {
                sWorkoutCurrent =  ActivityHistory.workouttypeAt(i);
                iDurationCurrent = ActivityHistory.durationAt(i);
                fDistanceCurrent = ActivityHistory.distanceAt(i);

                if (sWorkoutCurrent === "" || iDurationCurrent === 0 || fDistanceCurrent === 0)
                    continue;

                SharedResources.arrayLookupWorkoutFilterMainPageTableByName[sWorkoutCurrent].iDistance = SharedResources.arrayLookupWorkoutFilterMainPageTableByName[sWorkoutCurrent].iDistance + fDistanceCurrent;
                SharedResources.arrayLookupWorkoutFilterMainPageTableByName[sWorkoutCurrent].iDuration = SharedResources.arrayLookupWorkoutFilterMainPageTableByName[sWorkoutCurrent].iDuration + iDurationCurrent;
                SharedResources.arrayLookupWorkoutFilterMainPageTableByName[sWorkoutCurrent].iWorkouts++;

                SharedResources.arrayLookupWorkoutFilterMainPageTableByName["allworkouts"].iDistance = SharedResources.arrayLookupWorkoutFilterMainPageTableByName["allworkouts"].iDistance + fDistanceCurrent;
                SharedResources.arrayLookupWorkoutFilterMainPageTableByName["allworkouts"].iDuration = SharedResources.arrayLookupWorkoutFilterMainPageTableByName["allworkouts"].iDuration + iDurationCurrent;
                SharedResources.arrayLookupWorkoutFilterMainPageTableByName["allworkouts"].iWorkouts++;
            }

            fncSetWorkoutFilter();

            fncCheckAutosave();
        }
    }

    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: mainPage.height
        contentWidth: mainPage.width
        id: main_flickable

        PullDownMenu
        {
            id: menu
            MenuItem
            {
                text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
            MenuItem
            {
                text: qsTr("Settings")
                onClicked: pageStack.push(Qt.resolvedUrl("SettingsMenu.qml"))
            }
            MenuItem
            {
                text: qsTr("My Strava Activities")
                visible: stravaOAuth.linked
                onClicked: {

                    var dialog = pageStack.push(Qt.resolvedUrl("MyStravaActivities.qml"));
                }
            }
            MenuItem
            {
                text: qsTr("Start new workout")
                onClicked: pageStack.push(Qt.resolvedUrl("PreRecordPage.qml"))
            }
        }

        ColumnLayout
        {
            spacing: 0
            anchors.fill: parent
            width: parent.width

            PageHeader
            {
                id: pageHeader
                title: "Kuri"

                RowLayout
                {
                    id: filterSelection
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: Theme.paddingLarge

                    width: parent.width
                    spacing: Theme.paddingSmall

                    visible: ActivityHistory.varyingWorkoutTypes || (cmbWorkoutFilter.currentIndex !== 0 && historyList.count === 0)

                    Item
                    {
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        height: Theme.iconSizeSmallPlus
                        width: Theme.iconSizeSmallPlus

                        Image
                        {
                            anchors.fill: parent
                            fillMode: Image.PreserveAspectFit
                            source: SharedResources.arrayWorkoutTypesFilterMainPage[cmbWorkoutFilter.currentIndex].icon
                        }
                    }

                    ComboBox
                    {
                        id: cmbWorkoutFilter
                        Layout.alignment: Qt.AlignBaseline | Qt.AlignLeft
                        Layout.fillWidth: true
                        menu: ContextMenu
                        {
                            Repeater
                            {
                                id: idRepeaterFilterWorkout
                                model: SharedResources.arrayWorkoutTypesFilterMainPage;
                                MenuItem { text: modelData.labeltext }
                            }
                        }
                        Component.onCompleted: {
                            currentIndex = SharedResources.arrayWorkoutTypesFilterMainPage.map(function(e) {
                                return e.name;
                            }).indexOf(Settings.workoutTypeMainPage);

                            currentIndexChanged.connect(function() {
                                Settings.workoutTypeMainPage = SharedResources.arrayWorkoutTypesFilterMainPage[currentIndex].name;
                                fncSetWorkoutFilter();
                            });
                        }
                    }
                }
            }

            GridLayout
            {
                id: mainHeader
                width: parent.width
                columns: 2
                rowSpacing: 0
                columnSpacing: Theme.paddingSmall

                visible: !progressBarWaitLoadGPX.visible

                Item
                {
                    Layout.fillWidth: true
                    height: Theme.iconSizeMedium
                    Image
                    {
                        anchors.fill: parent
                        fillMode: Image.PreserveAspectFit
                        source: "../icons/journey-yellow.png"
                    }
                }
                Item
                {
                    Layout.fillWidth: true
                    height: Theme.iconSizeMedium
                    Image
                    {
                        anchors.fill: parent
                        fillMode: Image.PreserveAspectFit
                        source: "../icons/time-green.png"
                    }
                }
                Item
                {
                    Layout.fillWidth: true
                    height: Theme.iconSizeMedium
                    visible: false
                    Image
                    {
                        anchors.fill: parent
                        fillMode: Image.PreserveAspectFit
                        source: "../icons/fire-orange.png"
                    }
                }

                Item
                {
                    Layout.fillWidth: true
                    height: Theme.fontSizeSmall * 2
                    Label
                    {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        truncationMode: TruncationMode.Fade

                        text: sWorkoutDistance
                        font.pixelSize: Theme.fontSizeSmall
                        color: Theme.primaryColor
                    }
                }
                Item
                {
                    Layout.fillWidth: true
                    height: Theme.fontSizeSmall * 2
                    Label
                    {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        truncationMode: TruncationMode.Fade

                        text: sWorkoutDuration
                        font.pixelSize: Theme.fontSizeSmall
                        color: Theme.primaryColor
                    }
                }
                Item
                {
                    Layout.fillWidth: true
                    height: Theme.fontSizeSmall * 2
                    visible: false
                    Label
                    {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        truncationMode: TruncationMode.Fade

                        text: "here comes the calories"
                        font.pixelSize: Theme.fontSizeSmall
                        color: Theme.primaryColor
                    }
                }
                Item {
                    Layout.fillWidth: true
                    Layout.columnSpan: 2
                    height: Theme.fontSizeMedium * 2
                    Label
                    {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        truncationMode: TruncationMode.Fade

                        text: qsTr("%1 workouts").arg(historyList.count)
                        font.pixelSize: Theme.fontSizeMedium
                        color: Theme.primaryColor
                    }
                }
            }

            ProgressBar
            {
                id: progressBarWaitLoadGPX
                width: parent.width
                maximumValue: ActivityHistory.numberOfTracksToLoad
                valueText: qsTr("%1 of %2").arg(value).arg(maximumValue)
                label: qsTr("Loading GPX files...")
                value: ActivityHistory.numberOfLoadedTracks
                visible: (maximumValue > 0) && (value != maximumValue)
            }

            Separator
            {
                color: Theme.highlightColor
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
            }

            SilicaListView
            {
                Layout.fillHeight: true
                width: parent.width
                id: historyList
                clip: true
                model: ActivityHistory

                delegate: ListItem
                {
                    id: listItem
                    width: parent.width
                    ListView.onRemove: animateRemoval()
                    menu: ContextMenu
                    {
                        MenuItem
                        {
                            text: qsTr("Remove workout")
                            onClicked: remorseAction(qsTr("Removing workout..."), listItem.deleteTrack)

                        }
                        MenuItem
                        {
                            text: qsTr("Edit workout")
                            onClicked:
                            {
                                console.log("Filename: " + filename);
                                console.log("name: " + name);
                                console.log("type: " + workout);
                                console.log("description: " + description);

                                iCurrentWorkout = SharedResources.fncGetIndexByName(workout);

                                var dialog = pageStack.push(id_Dialog_EditWorkout);
                                dialog.sName = name;
                                dialog.sDesc = description;
                                dialog.iWorkout = SharedResources.fncGetIndexByName(workout);

                                dialog.accepted.connect(function()
                                {
                                    //Edit and save GPX file
                                    trackLoader.vReadFile(filename);
                                    trackLoader.vSetNewProperties(name, description, workout, dialog.sName, dialog.sDesc, dialog.sWorkout)
                                    trackLoader.vWriteFile(filename);

                                    model.name = dialog.sName;
                                    model.description = dialog.sDesc;
                                    model.workout = dialog.sWorkout;
                                })
                            }
                        }
                        MenuItem
                        {
                            text: qsTr("Send to Sports-Tracker.com")
                            Component.onCompleted: visible = Settings.stUsername !== "";
                            onClicked: {
                                trackLoader.filename = filename;
                                var dialog = pageStack.push(Qt.resolvedUrl("SportsTrackerUploadPage.qml"),{ stcomment: description});
                                dialog.accepted.connect(function() {
                                    detail_busy.running = true;
                                    detail_busy.visible = true;
                                    load_text.visible = true;
                                    main_flickable.visible = false;
                                    ST.uploadToSportsTracker(dialog.sharing*1, dialog.stcomment, displayNotification);
                                });
                            }
                        }
                    }

                    function deleteTrack()
                    {
                        ActivityHistory.removeTrack(index);
                    }

                    GridLayout
                    {
                        anchors.fill: parent
                        anchors.leftMargin: Theme.paddingMedium
                        anchors.rightMargin: Theme.paddingMedium
                        columns: 2
                        rowSpacing: 0
                        columnSpacing: Theme.paddingMedium
                        Item
                        {
                            Layout.rowSpan: 2
                            Layout.fillHeight: true
                            width: Theme.iconSizeSmall * 1.5

                            Image
                            {
                                anchors.fill: parent
                                fillMode: Image.PreserveAspectFit
                                source: workout==="" ? "" : SharedResources.arrayLookupWorkoutTableByName[workout].icon
                            }
                        }
                        Item
                        {
                            Layout.fillWidth: true
                            Layout.fillHeight: true

                            RowLayout
                            {
                                anchors.fill: parent
                                spacing: Theme.paddingMedium

                                Text
                                {
                                    Layout.fillWidth: true
                                    height: parent.height
                                    horizontalAlignment: Text.AlignLeft
                                    verticalAlignment: Text.AlignVCenter
                                    elide: Text.ElideRight

                                    text: dateTime
                                    font.pixelSize: Theme.fontSizeSmall
                                    color: Theme.highlightColor
                                }
                                Text
                                {
                                    horizontalAlignment: Text.AlignRight
                                    verticalAlignment: Text.AlignVCenter

                                    text: duration
                                    font.pixelSize: Theme.fontSizeExtraSmall
                                    color: "green"
                                }
                                Text
                                {
                                    horizontalAlignment: Text.AlignRight
                                    verticalAlignment: Text.AlignVCenter

                                    font.pixelSize: Theme.fontSizeExtraSmall
                                    color: Theme.highlightColor
                                    Component.onCompleted: {
                                        text = (Settings.measureSystem === 0)
                                            ? (distance/1000).toFixed(2) + " " + qsTr("km")
                                            : JSTools.fncConvertDistanceToImperial(distance/1000).toFixed(2) + " " + qsTr("mi")
                                    }
                                }
                            }
                        }
                        Item
                        {
                            Layout.fillWidth: true
                            Layout.fillHeight: true

                            RowLayout
                            {
                                anchors.fill: parent
                                spacing: Theme.paddingMedium

                                Text
                                {
                                    Layout.fillWidth: true
                                    height: parent.height
                                    horizontalAlignment: Text.AlignLeft
                                    verticalAlignment: Text.AlignVCenter
                                    elide: Text.ElideRight

                                    text: name==="" ? SharedResources.arrayLookupWorkoutTableByName[workout].labeltext : name
                                    font.pixelSize: Theme.fontSizeExtraSmall
                                    color: Theme.primaryColor
                                }
                                Text
                                {
                                    horizontalAlignment: Text.AlignRight
                                    verticalAlignment: Text.AlignVCenter

                                    font.pixelSize: Theme.fontSizeExtraSmall
                                    color: Theme.secondaryColor
                                    Component.onCompleted: {
                                        text = (Settings.measureSystem === 0)
                                            ? speed.toFixed(1) + " " + qsTr("km/h")
                                            : JSTools.fncConvertSpeedToImperial(speed).toFixed(1) + " " + qsTr("mi/h")
                                    }
                                }
                            }
                        }
                    }

                    onClicked: pageStack.push(Qt.resolvedUrl("DetailedViewPage.qml"), {track: model})
                }
            }
        }
    }


    Component
    {
        id: id_Dialog_Autosave

        Dialog
        {
            width: parent.width
            canAccept: true
            acceptDestination: mainPage
            acceptDestinationAction: PageStackAction.Pop


            Flickable
            {
                width: parent.width
                height: parent.height
                interactive: false

                Column
                {
                    width: parent.width

                    DialogHeader
                    {
                        title: qsTr("Uncompleted workout found!")
                        defaultAcceptText: qsTr("Resume")
                    }
                }
            }
        }
    }
    Component
    {
        id: id_Dialog_EditWorkout


        Dialog
        {
            property string sName
            property string sDesc
            property int iWorkout
            property string sWorkout

            canAccept: true
            acceptDestination: mainPage
            acceptDestinationAction:
            {
                sName = id_TXF_WorkoutName.text;
                sDesc = id_TXF_WorkoutDesc.text;
                iWorkout = cmbWorkout.currentIndex;

                PageStackAction.Pop;
            }

            Flickable
            {
                width: parent.width
                height: parent.height
                interactive: false

                Column
                {
                    width: parent.width

                    DialogHeader { title: qsTr("Edit workout") }

                    TextField
                    {
                        id: id_TXF_WorkoutName
                        width: parent.width
                        label: qsTr("Workout name")
                        placeholderText: qsTr("Workout name")
                        text: sName
                        inputMethodHints: Qt.ImhNoPredictiveText
                        focus: true
                        horizontalAlignment: TextInput.AlignLeft
                    }
                    Item
                    {
                        width: parent.width
                        height: Theme.paddingLarge
                    }
                    TextField
                    {
                        id: id_TXF_WorkoutDesc
                        width: parent.width
                        label: qsTr("Workout description")
                        placeholderText: qsTr("Workout description")
                        text: sDesc
                        inputMethodHints: Qt.ImhNoPredictiveText
                        focus: true
                        horizontalAlignment: TextInput.AlignLeft
                    }
                    Item
                    {
                        width: parent.width
                        height: Theme.paddingLarge
                    }
                    Row
                    {
                        spacing: Theme.paddingSmall
                        width:parent.width

                        Image
                        {
                            id: imgDialogWorkoutImage
                            height: parent.width / 8
                            width: parent.width / 8
                            fillMode: Image.PreserveAspectFit
                        }
                        ComboBox
                        {
                            id: cmbWorkout
                            width: (parent.width / 8) * 7
                            label: qsTr("Workout:")
                            currentIndex: iCurrentWorkout
                            menu: ContextMenu
                            {
                                Repeater
                                {
                                    model: SharedResources.arrayWorkoutTypes;
                                    MenuItem { text: modelData.labeltext }
                                }
                            }
                            onCurrentItemChanged:
                            {
                                console.log("Workout changed!");

                                imgDialogWorkoutImage.source = SharedResources.arrayWorkoutTypes[currentIndex].icon;
                                iWorkout = currentIndex;
                                sWorkout = SharedResources.arrayWorkoutTypes[currentIndex].name;
                            }
                        }
                    }
                }
            }
        }
    }
}
