/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0

import harbour.kuri 1.0

import "../tools/JSTools.js" as JSTools

Page
{
    id: page
    allowedOrientations: Orientation.Portrait

    property var indexValues: [10, 8, 3]

    Component.onCompleted:
    {
        console.log("First Active CoverSettingsPage");

        var arraySize = JSTools.arrayPebbleValueTypes.length;

        var arValueTypes = Settings.valueCoverFields.split(",");
        if (arValueTypes !== undefined && arValueTypes !== "" && arValueTypes.length === indexValues.length)
        {
            var index0 = parseInt(arValueTypes[0]);
            index0 = (index0 >= 0 && index0 < arraySize) ? index0 : indexValues[0];

            var index1 = parseInt(arValueTypes[1]);
            index1 = (index1 >= 0 && index1 < arraySize) ? index1 : indexValues[1];

            var index2 = parseInt(arValueTypes[2]);
            index2 = (index2 >= 0 && index2 < arraySize) ? index2 : indexValues[2];

            if (uniqueNonZeroValues(index0, index1, index2))
            {
                indexValues[0] = index0;
                indexValues[1] = index1;
                indexValues[2] = index2;
            }
        }

        id_CMB_ValueField1.currentIndex = indexValues[0];
        id_CMB_ValueField2.currentIndex = indexValues[1];
        id_CMB_ValueField3.currentIndex = indexValues[2];

        id_CMB_ValueField1.currentIndexChanged.connect(handleValueChange);
        id_CMB_ValueField2.currentIndexChanged.connect(handleValueChange);
        id_CMB_ValueField3.currentIndexChanged.connect(handleValueChange);

        JSTools.fncGenerateHelperArrayCoverPage();
    }

    function uniqueNonZeroValues(index0, index1, index2) {
        if(index0 !== 0 && index0 === index1) { return false; }
        if(index1 !== 0 && index1 === index2) { return false; }
        if(index2 !== 0 && index2 === index0) { return false; }
        return true;
    }

    function handleValueChange() {
        var index0 = id_CMB_ValueField1.currentIndex;
        var index1 = id_CMB_ValueField2.currentIndex;
        var index2 = id_CMB_ValueField3.currentIndex;

        // sanity check for unique values
        if (!uniqueNonZeroValues(index0, index1, index2))
        {
            fncShowMessage("Error",qsTr("This value is already assigned!"), 3000);

            id_CMB_ValueField1.currentIndex = indexValues[0];
            id_CMB_ValueField2.currentIndex = indexValues[1];
            id_CMB_ValueField3.currentIndex = indexValues[2];
            return
        }

        Settings.valueCoverFields = index0.toString() + "," + index1.toString() + "," + index2.toString();

        indexValues[0] = index0;
        indexValues[1] = index1;
        indexValues[2] = index2;

        JSTools.fncGenerateHelperArrayCoverPage();
    }

    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: column.height + Theme.paddingLarge;
        VerticalScrollDecorator {}

        Column
        {
            id: column
            width: page.width
            spacing: Theme.paddingLarge
            PageHeader
            {
                title: qsTr("App cover settings")
            }
            Label
            {
                width: parent.width
                text: qsTr("Select values to be shown on the App cover. The values are also used for the lock screen on the record page.")
                font.pixelSize: Theme.fontSizeSmall
                wrapMode: Text.WordWrap
            }
            ComboBox
            {
                id: id_CMB_ValueField1
                label: qsTr("First field:")
                menu: ContextMenu { Repeater { model: JSTools.arrayPebbleValueTypes; MenuItem { text: modelData.header } }}
            }
            ComboBox
            {
                id: id_CMB_ValueField2
                label: qsTr("Second field:")
                menu: ContextMenu { Repeater { model: JSTools.arrayPebbleValueTypes; MenuItem { text: modelData.header } }}
            }
            ComboBox
            {
                id: id_CMB_ValueField3
                label: qsTr("Third field:")
                menu: ContextMenu { Repeater { model: JSTools.arrayPebbleValueTypes; MenuItem { text: modelData.header } }}
            }
        }
    }
}
