/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0

import harbour.kuri 1.0

import "../tools/JSTools.js" as JSTools

Page
{
    id: page
    allowedOrientations: Orientation.Portrait

    property var indexValues: [9, 8, 5, 3]

    Component.onCompleted:
    {
        console.log("First Active VoiceCycleDurationSettingsPage");

        var arraySize = JSTools.arrayVoiceValueTypes.length;

        var arValueTypes = Settings.voiceCycDurationFields.split(",");
        if (arValueTypes !== undefined && arValueTypes !== "" && arValueTypes.length === indexValues.length)
        {
            var index0 = parseInt(arValueTypes[0]);
            index0 = (index0 >= 0 && index0 < arraySize) ? index0 : indexValues[0];

            var index1 = parseInt(arValueTypes[1]);
            index1 = (index1 >= 0 && index1 < arraySize) ? index1 : indexValues[1];

            var index2 = parseInt(arValueTypes[2]);
            index2 = (index2 >= 0 && index2 < arraySize) ? index2 : indexValues[2];

            var index3 = parseInt(arValueTypes[3]);
            index3 = (index3 >= 0 && index3 < arraySize) ? index3 : indexValues[3];

            if (uniqueNonZeroValues(index0, index1, index2, index3))
            {
                indexValues[0] = index0;
                indexValues[1] = index1;
                indexValues[2] = index2;
                indexValues[3] = index3;
            }
        }

        id_CMB_ValueField1.currentIndex = indexValues[0];
        id_CMB_ValueField2.currentIndex = indexValues[1];
        id_CMB_ValueField3.currentIndex = indexValues[2];
        id_CMB_ValueField4.currentIndex = indexValues[3];

        id_CMB_ValueField1.currentIndexChanged.connect(handleValueChange);
        id_CMB_ValueField2.currentIndexChanged.connect(handleValueChange);
        id_CMB_ValueField3.currentIndexChanged.connect(handleValueChange);
        id_CMB_ValueField4.currentIndexChanged.connect(handleValueChange);

        JSTools.fncGenerateHelperArrayFieldIDDuration();
    }

    function uniqueNonZeroValues(index0, index1, index2, index3) {
        if(index0 !== 0 && index0 === index1) { return false; }
        if(index0 !== 0 && index0 === index2) { return false; }
        if(index0 !== 0 && index0 === index3) { return false; }
        if(index1 !== 0 && index1 === index2) { return false; }
        if(index1 !== 0 && index1 === index3) { return false; }
        if(index2 !== 0 && index2 === index3) { return false; }
        return true;
    }

    function handleValueChange() {
        var index0 = id_CMB_ValueField1.currentIndex;
        var index1 = id_CMB_ValueField2.currentIndex;
        var index2 = id_CMB_ValueField3.currentIndex;
        var index3 = id_CMB_ValueField4.currentIndex;

        // sanity check for unique values
        if (!uniqueNonZeroValues(index0, index1, index2, index3))
        {
            fncShowMessage("Error",qsTr("This value is already assigned!"), 3000);

            id_CMB_ValueField1.currentIndex = indexValues[0];
            id_CMB_ValueField2.currentIndex = indexValues[1];
            id_CMB_ValueField3.currentIndex = indexValues[2];
            id_CMB_ValueField4.currentIndex = indexValues[3];
            return
        }

        Settings.voiceCycDurationFields = index0.toString() + "," + index1.toString() + "," + index2.toString() + "," + index3.toString();

        indexValues[0] = index0;
        indexValues[1] = index1;
        indexValues[2] = index2;
        indexValues[3] = index3;

        JSTools.fncGenerateHelperArrayFieldIDDuration();
    }

    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: column.height + Theme.paddingLarge;
        VerticalScrollDecorator {}
        Column
        {
            id: column
            width: page.width
            spacing: Theme.paddingLarge
            PageHeader
            {
                title: qsTr("Regular announcements by duration")
            }
            TextSwitch
            {
                id: id_TextSwitch_IntervalDuration
                text: qsTr("Enabled")
                Component.onCompleted: {
                    checked = Settings.voiceCycDurationEnable
                    checkedChanged.connect(function() {
                        Settings.voiceCycDurationEnable = checked;
                    });
                }
            }
            ComboBox
            {
                id: id_CMB_IntervalDuration
                label: qsTr("Every ")
                visible: id_TextSwitch_IntervalDuration.checked
                menu: ContextMenu
                {
                    MenuItem { text: qsTr("30 seconds") }
                    MenuItem { text: qsTr("1 minute") }
                    MenuItem { text: qsTr("2 minutes") }
                    MenuItem { text: qsTr("5 minutes") }
                    MenuItem { text: qsTr("10 minutes") }
                    MenuItem { text: qsTr("20 minutes") }
                    MenuItem { text: qsTr("hour") }
                }
                Component.onCompleted: {
                    var voiceCycDuration = Settings.voiceCycDuration;
                    if (voiceCycDuration === 30)
                        currentIndex = 0;
                    else if (voiceCycDuration === 60)
                        currentIndex = 1;
                    else if (voiceCycDuration === 120)
                        currentIndex = 2;
                    else if (voiceCycDuration === 300)
                        currentIndex = 3;
                    else if (voiceCycDuration === 600)
                        currentIndex = 4;
                    else if (voiceCycDuration === 1200)
                        currentIndex = 5;
                    else if (voiceCycDuration === 3600)
                        currentIndex = 6;
                    else if (voiceCycDuration === 60)
                        currentIndex = 1;

                    currentIndexChanged.connect(function() {
                        if (currentIndex === 0)
                            Settings.voiceCycDuration = 30;
                        else if (currentIndex === 1)
                            Settings.voiceCycDuration = 60;
                        else if (currentIndex === 2)
                            Settings.voiceCycDuration = 120;
                        else if (currentIndex === 3)
                            Settings.voiceCycDuration = 300;
                        else if (currentIndex === 4)
                            Settings.voiceCycDuration = 600;
                        else if (currentIndex === 5)
                            Settings.voiceCycDuration = 1200;
                        else if (currentIndex === 6)
                            Settings.voiceCycDuration = 3600;
                        else
                            Settings.voiceCycDuration = 60
                    });
                }
            }
            Separator
            {
                visible: id_TextSwitch_IntervalDuration.checked
                color: Theme.highlightColor
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
            }
            TextSwitch
            {
                visible: id_TextSwitch_IntervalDuration.checked
                text: qsTr("Play value announcement")
                description: qsTr("Before a value is played, the type of value is announced e.g \"Distance:\". This makes the voice announcement last longer.")
                Component.onCompleted: {
                    checked = Settings.voiceCycDurationHeadlineEnable
                    checkedChanged.connect(function() {
                        Settings.voiceCycDurationHeadlineEnable = checked;
                    });
                }
            }
            Separator
            {
                visible: id_TextSwitch_IntervalDuration.checked
                color: Theme.highlightColor
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
            }
            ComboBox
            {
                visible: id_TextSwitch_IntervalDuration.checked
                id: id_CMB_ValueField1
                label: qsTr("1 announcement:")
                menu: ContextMenu { Repeater { model: JSTools.arrayVoiceValueTypes; MenuItem { text: modelData.header } }}
            }
            ComboBox
            {
                visible: id_TextSwitch_IntervalDuration.checked
                id: id_CMB_ValueField2
                label: qsTr("2 announcement:")
                menu: ContextMenu { Repeater { model: JSTools.arrayVoiceValueTypes; MenuItem { text: modelData.header } }}
            }
            ComboBox
            {
                visible: id_TextSwitch_IntervalDuration.checked
                id: id_CMB_ValueField3
                label: qsTr("3 announcement:")
                menu: ContextMenu { Repeater { model: JSTools.arrayVoiceValueTypes; MenuItem { text: modelData.header } }}
            }
            ComboBox
            {
                visible: id_TextSwitch_IntervalDuration.checked
                id: id_CMB_ValueField4
                label: qsTr("4 announcement:")
                menu: ContextMenu { Repeater { model: JSTools.arrayVoiceValueTypes; MenuItem { text: modelData.header } }}
            }
        }
    }
}
