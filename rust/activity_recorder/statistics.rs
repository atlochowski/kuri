// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use super::PositionInfo;

use std::collections::VecDeque;

pub struct Value<T> {
    value: T,
    changed: bool,
}

impl<T> Value<T> {
    fn new(value: T) -> Self {
        Self {
            value,
            changed: true,
        }
    }

    fn set(&mut self, value: T) {
        self.value = value;
        self.changed = true;
    }

    pub fn when_changed<F>(&mut self, f: F)
    where
        F: FnOnce(&T),
    {
        if self.changed {
            f(&self.value);
            self.changed = false;
        }
    }
}

pub struct Statistics {
    timestamp_last_tick: i64,
    timestamp_recording_start: i64,
    timestamp_section_start: i64,
    recording_start_pending: bool,
    pub recording: Value<bool>,
    pub section_count: Value<u64>,
    pub duration_in_section: Value<u64>,
    // queue for position data; will be emptied with each position update when not recording so that only one sample will be available
    // maybe just use a Option<PositionInfo> and Option<HeartRate> ... on the other hand, this is needed to calculate the pace for e.g. the last 100m
    position_info: VecDeque<PositionInfo>,
    timestamp_last_heart_rate_update: i64,
    heart_rate: u32,
}

impl Default for Statistics {
    fn default() -> Self {
        Self {
            timestamp_last_tick: 0,
            timestamp_recording_start: 0,
            timestamp_section_start: 0,
            recording_start_pending: false,
            recording: Value::new(false),
            section_count: Value::new(1),
            duration_in_section: Value::new(0),
            position_info: VecDeque::with_capacity(1000),
            timestamp_last_heart_rate_update: 0,
            heart_rate: 0,
        }
    }
}

impl Statistics {
    pub fn start(&mut self) {
        self.recording_start_pending = true;
    }

    pub fn stop(&mut self) {
        self.recording_start_pending = false;
        *self = Statistics::default();
    }

    pub fn pause(&mut self) {
        println!("#### TODO: Statistics::pause");
    }

    pub fn proceed(&mut self) {
        println!("#### TODO: Statistics::proceed");
    }

    pub fn next_section(&mut self, _timestamp: i64) {
        self.timestamp_section_start = self.timestamp_last_tick;
        self.section_count.set(self.section_count.value + 1);
        self.duration_in_section.set(0);
    }

    pub fn position_update(&mut self, timestamp: i64, position_info: PositionInfo) {
        self.position_info.push_back(position_info);
        if self.position_info.len() > 2 {
            self.position_info.pop_front();
        }

        self.tick(timestamp);
    }

    pub fn heart_rate_update(&mut self, timestamp: i64, heart_rate: u32) {
        self.timestamp_last_heart_rate_update = timestamp;
        self.heart_rate = heart_rate;
    }

    pub fn tick(&mut self, timestamp: i64) {
        self.timestamp_last_tick = timestamp;

        if self.recording_start_pending {
            self.recording_start_pending = false;
            self.recording.set(true);
            self.timestamp_recording_start = timestamp;
            self.timestamp_section_start = timestamp;
        }
        if self.recording.value {
            self.duration_in_section
                .set((timestamp - self.timestamp_section_start) as u64);
        }
    }
}
