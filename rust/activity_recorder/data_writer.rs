// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use super::PositionInfo;

use chrono::{Local, TimeZone};
use dirs::data_dir;
use serde::{Deserialize, Serialize};

use std::fs::{File, OpenOptions};
use std::io::{BufReader, BufWriter, Write};
use std::path::PathBuf;

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
#[serde(rename_all = "kebab-case")]
pub struct Position {
    pub latitude: f64,
    pub longitude: f64,
    pub altitude: f32,
    pub horizontal_accuracy: u16,
    pub vertical_accuracy: u16,
}

impl From<PositionInfo> for Position {
    fn from(position_info: PositionInfo) -> Self {
        Self {
            latitude: position_info.latitude,
            longitude: position_info.longitude,
            altitude: position_info.altitude,
            horizontal_accuracy: position_info.horizontal_accuracy,
            vertical_accuracy: position_info.horizontal_accuracy,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
#[serde(rename_all = "kebab-case")]
struct Measurement {
    timestamp: i64,
    #[serde(flatten)]
    position: Option<Position>,
    heart_rate: Option<u32>,
}

pub(super) trait Storage: Write {
    type Backend: Write;
    fn new_storage(data_dir: PathBuf, file_name: &str) -> Option<BufWriter<Self::Backend>>;
    fn make_persistent(old_path: PathBuf, new_path: PathBuf);
    fn discard(path: PathBuf);
}

impl Storage for File {
    type Backend = Self;

    fn new_storage(data_dir: PathBuf, file_name: &str) -> Option<BufWriter<Self>> {
        std::fs::create_dir_all(data_dir.as_path())
            .map_err(|e| println!("Error creating data dir path: {}", e))
            .ok()?;
        let mut path = data_dir;
        path.push(file_name);

        let file = OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(path)
            .map_err(|e| println!("Error creating recording file: {}", e))
            .ok()?;
        Some(BufWriter::new(file))
    }

    fn make_persistent(old_path: PathBuf, new_path: PathBuf) {
        let old_file = OpenOptions::new()
            .read(true)
            .open(old_path.clone())
            .map_err(|e| println!("Error reading recording file: {}", e))
            .ok();

        let new_file = OpenOptions::new()
            .create_new(true)
            .truncate(true)
            .write(true)
            .open(new_path)
            .map_err(|e| println!("Error creating persistent recording file: {}", e))
            .ok();

        if let (Some(old_file), Some(new_file)) = (old_file, new_file) {
            zstd::stream::copy_encode(BufReader::new(old_file), BufWriter::new(new_file), 0)
                .map_err(|e| println!("Error making recording persistent: {}", e))
                .map(|_| Self::discard(old_path))
                .ok();
        }
    }

    fn discard(path: PathBuf) {
        std::fs::remove_file(path)
            .map_err(|e| println!("Error discarding recording: {}", e))
            .ok();
    }
}

pub(super) struct DataWriter<S: Storage> {
    data_dir_path: PathBuf,
    data_file: Option<BufWriter<S>>,
    timestamp_last_tick: i64,
    timestamp_recording_start: i64,
    recording_start_pending: bool,
    recording: bool,
    position_info: Option<PositionInfo>,
    timestamp_last_heart_rate_update: i64,
    heart_rate: u32,
}

impl<S: Storage<Backend = S>> DataWriter<S> {
    const KURI_PATH: &'static str = "org.kuri/kuri/";
    const CURRENT_RECORDING_FILE_NAME: &'static str = "recording.data-raw";

    pub fn new() -> Self {
        let mut data_dir_path = data_dir().expect("Path to data dir");
        data_dir_path.push(Self::KURI_PATH);

        Self {
            data_dir_path,
            data_file: None,
            timestamp_last_tick: 0,
            timestamp_recording_start: 0,
            recording_start_pending: false,
            recording: false,
            position_info: None,
            timestamp_last_heart_rate_update: 0,
            heart_rate: 0,
        }
    }

    pub fn save_recording(&mut self) {
        if self.timestamp_recording_start == 0 {
            return;
        }

        let mut old_path = self.data_dir_path.clone();
        old_path.push(Self::CURRENT_RECORDING_FILE_NAME);
        let mut new_path = self.data_dir_path.clone();

        let file_name = format!(
            "{}-1.data-raw.kuri.zst",
            Local
                .timestamp(self.timestamp_recording_start, 0)
                .format("%Y%m%d-%H%M%S")
        );
        new_path.push(&file_name);
        S::make_persistent(old_path, new_path);
    }

    pub fn discard_recording(&mut self) {
        let mut path = self.data_dir_path.clone();
        path.push(Self::CURRENT_RECORDING_FILE_NAME);
        S::discard(path);
    }

    pub fn start(&mut self) {
        self.data_file = S::new_storage(
            self.data_dir_path.clone(),
            Self::CURRENT_RECORDING_FILE_NAME,
        );

        self.data_file.as_mut().map(|buf| {
            writeln!(buf, "[info]")
                .map_err(|e| println!("Error occured: {:?}", e))
                .ok();
            writeln!(buf, "version = {}", 1)
                .map_err(|e| println!("Error occured: {:?}", e))
                .ok();
            writeln!(buf, "source = '{}'", "kuri")
                .map_err(|e| println!("Error occured: {:?}", e))
                .ok();
        });

        self.recording_start_pending = true;
    }

    pub fn stop(&mut self) -> Option<S> {
        self.recording_start_pending = false;
        self.recording = false;
        self.data_file.take().and_then(|buf| buf.into_inner().ok())
    }

    pub fn pause(&mut self) {
        println!("#### TODO: DataWriter::pause");
    }

    pub fn proceed(&mut self) {
        println!("#### TODO: DataWriter::proceed");
    }

    pub fn next_section(&mut self, _timestamp: i64) {
        self.data_file.as_mut().map(|buf| {
            writeln!(buf, "[[section]]")
                .map_err(|e| println!("Error occured: {:?}", e))
                .ok();
        });

        self.tick(self.timestamp_last_tick);
    }

    pub fn position_update(&mut self, timestamp: i64, position_info: PositionInfo) {
        self.position_info = Some(position_info);
        self.tick(timestamp);
    }

    pub fn heart_rate_update(&mut self, timestamp: i64, heart_rate: u32) {
        self.timestamp_last_heart_rate_update = timestamp;
        self.heart_rate = heart_rate;
    }

    pub fn tick(&mut self, timestamp: i64) {
        self.timestamp_last_tick = timestamp;

        if self.recording_start_pending {
            self.recording_start_pending = false;
            self.recording = true;
            self.timestamp_recording_start = timestamp;

            let start_time = Local.timestamp(timestamp, 0).format("%+");
            self.data_file.as_mut().map(|buf| {
                writeln!(buf, "start-time = '{}'", start_time)
                    .map_err(|e| println!("Error occured: {:?}", e))
                    .ok();
                writeln!(buf, "")
                    .map_err(|e| println!("Error occured: {:?}", e))
                    .ok();
                writeln!(buf, "[[section]]")
                    .map_err(|e| println!("Error occured: {:?}", e))
                    .ok();
            });
        }
        if self.recording {
            let mut measurement = Measurement {
                timestamp,
                position: None,
                heart_rate: None,
            };
            self.position_info.map(|pos_info| {
                if pos_info.timestamp == timestamp {
                    measurement.position = Some(pos_info.into());
                }
            });
            if self.heart_rate != 0 && timestamp - self.timestamp_last_heart_rate_update < 3 {
                measurement.heart_rate = Some(self.heart_rate);
            }

            self.data_file.as_mut().map(|buf| {
                writeln!(buf, "[[section.measurement]]")
                    .map_err(|e| println!("Error occured: {:?}", e))
                    .ok();
                writeln!(buf, "{}", toml::to_string_pretty(&measurement).unwrap())
                    .map_err(|e| println!("Error occured: {:?}", e))
                    .ok();
            });
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    // TODO this should be moved to the data reader

    #[derive(Serialize, Deserialize, Debug)]
    #[serde(rename_all = "kebab-case")]
    struct Section {
        measurement: Vec<Measurement>,
    }

    #[derive(Serialize, Deserialize, Debug)]
    #[serde(rename_all = "kebab-case")]
    struct Info {
        version: u64,
        source: String,
        start_time: String,
    }

    #[derive(Serialize, Deserialize, Debug)]
    #[serde(rename_all = "kebab-case")]
    struct Record {
        info: Info,
        section: Vec<Section>,
    }

    const TIMESTAMP: i64 = 1123580000;

    impl Storage for Vec<u8> {
        type Backend = Self;

        fn new_storage(_data_dir: PathBuf, _file_name: &str) -> Option<BufWriter<Self>> {
            Some(BufWriter::new(Self::new()))
        }

        fn make_persistent(_old_path: PathBuf, _new_path: PathBuf) {}

        fn discard(_path: PathBuf) {}
    }

    fn minimal_header() -> String {
        format!(
            r#"[info]
version = {}
source = '{}'"#,
            1, "kuri",
        )
    }

    #[test]
    fn start_and_stop_without_data_creates_file_with_header_only() {
        let mut sut = DataWriter::<Vec<u8>>::new();

        sut.start();
        let file_content = String::from_utf8(sut.stop().unwrap()).unwrap();

        let expected_content = format!("{}\n", minimal_header());

        assert_eq!(file_content, expected_content);
    }

    #[test]
    fn start_and_stop_with_one_tick_ceates_file_with_one_section_and_timestamp_only() {
        let mut sut = DataWriter::<Vec<u8>>::new();

        sut.start();
        sut.tick(TIMESTAMP);
        let file_content = String::from_utf8(sut.stop().unwrap()).unwrap();

        sut.save_recording();

        let start_time = Local.timestamp(TIMESTAMP, 0).format("%+");

        let expected_content = format!(
            r#"{}
start-time = '{}'

[[section]]
[[section.measurement]]
timestamp = {}

"#,
            minimal_header(),
            start_time,
            TIMESTAMP
        );

        assert_eq!(file_content, expected_content);
    }

    #[test]
    fn start_and_stop_with_one_position_update_ceates_file_with_one_section_and_position_info() {
        let pos = PositionInfo {
            timestamp: TIMESTAMP,
            latitude: 13.31,
            longitude: 42.24,
            altitude: 73.37,
            horizontal_accuracy: 7,
            vertical_accuracy: 3,
        };

        let mut sut = DataWriter::<Vec<u8>>::new();

        sut.start();
        sut.position_update(TIMESTAMP, pos);
        let file_content = String::from_utf8(sut.stop().unwrap()).unwrap();

        sut.save_recording();

        let start_time = Local.timestamp(TIMESTAMP, 0).format("%+");

        let expected_content = format!(
            r#"{}
start-time = '{}'

[[section]]
[[section.measurement]]
timestamp = {}
{}
"#,
            minimal_header(),
            start_time,
            TIMESTAMP,
            toml::to_string_pretty::<Position>(&pos.into()).unwrap()
        );

        assert_eq!(file_content, expected_content);
    }

    #[test]
    fn start_and_stop_with_one_heart_rate_update_ceates_file_with_one_section_and_heart_rate() {
        const HEART_RATE: u32 = 111;

        let mut sut = DataWriter::<Vec<u8>>::new();

        sut.start();
        sut.heart_rate_update(TIMESTAMP, HEART_RATE);
        sut.tick(TIMESTAMP);
        let file_content = String::from_utf8(sut.stop().unwrap()).unwrap();

        sut.save_recording();

        let start_time = Local.timestamp(TIMESTAMP, 0).format("%+");

        let expected_content = format!(
            r#"{}
start-time = '{}'

[[section]]
[[section.measurement]]
timestamp = {}
heart-rate = {}

"#,
            minimal_header(),
            start_time,
            TIMESTAMP,
            HEART_RATE,
        );

        assert_eq!(file_content, expected_content);
    }

    #[test]
    fn incremental_serialization_produces_same_result_as_full_serialization() {
        let pos_info1 = PositionInfo {
            timestamp: TIMESTAMP,
            latitude: 13.3,
            longitude: 37.7,
            altitude: 111.1,
            horizontal_accuracy: 9,
            vertical_accuracy: 4,
        };

        let mut sut = DataWriter::<Vec<u8>>::new();

        sut.start();
        sut.position_update(TIMESTAMP, pos_info1);
        sut.tick(TIMESTAMP + 1);
        sut.next_section(TIMESTAMP + 1);
        sut.tick(TIMESTAMP + 2);
        let file_content = String::from_utf8(sut.stop().unwrap()).unwrap();

        sut.save_recording();

        let meas1 = Measurement {
            timestamp: TIMESTAMP,
            position: Some(pos_info1.into()),
            heart_rate: None,
        };
        let meas2 = Measurement {
            timestamp: TIMESTAMP + 1,
            position: None,
            heart_rate: None,
        };
        let meas3 = Measurement {
            timestamp: TIMESTAMP + 2,
            position: None,
            heart_rate: None,
        };

        let section = vec![
            Section {
                measurement: vec![meas1, meas2],
            },
            Section {
                measurement: vec![meas2, meas3],
            },
        ];

        let start_time = Local.timestamp(TIMESTAMP, 0).format("%+").to_string();

        let record = Record {
            info: Info {
                version: 1,
                source: "kuri".into(),
                start_time,
            },
            section,
        };

        let expected_content = toml::to_string_pretty(&record).unwrap() + "\n";

        assert_eq!(file_content, expected_content);
    }
}
