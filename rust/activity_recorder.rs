// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

mod data_writer;
use crate::activity_recorder::data_writer::DataWriter;

mod statistics;
use crate::activity_recorder::statistics::Statistics;

use chrono::Utc;
use crossbeam_channel::{bounded, Receiver, Sender};

use std::fs::File;
use std::thread::{self, JoinHandle};

pub use ffi::{ActivityRecorder, ActivityRecorderEvents, PositionInfo};

#[derive(Debug)]
enum Action {
    Start,
    Stop,
    Pause,
    Proceed,
    NextSection,
    PositionUpdate(PositionInfo),
    HeartRateUpdate(u32),
    Tick,
    SaveRecording,
    DiscardRecording,
}

#[derive(Debug)]
struct Command {
    timestamp: i64,
    action: Action,
}

pub struct ActivityRecorderController {
    sender: Option<Sender<Command>>,
    worker: Option<JoinHandle<()>>,
}

impl Drop for ActivityRecorderController {
    fn drop(&mut self) {
        self.sender = None;
        self.worker.take().map(|worker| {
            worker
                .join()
                .map_err(|e| {
                    println!("Error joining worker thread: {:?}", e);
                    e
                })
                .ok()
        });
    }
}

pub fn new_activity_recorder(events: ActivityRecorderEvents) -> ActivityRecorder {
    ActivityRecorder::new(events)
}

impl ActivityRecorder {
    fn new(events: ActivityRecorderEvents) -> Self {
        let (sender, receiver) = bounded(100);
        let worker = {
            let receiver = receiver.clone();
            thread::spawn(move || {
                Self::command_handler(receiver, events);
            })
        };
        Self {
            controller: Box::new(ActivityRecorderController {
                sender: Some(sender),
                worker: Some(worker),
            }),
        }
    }

    fn command_handler(receiver: Receiver<Command>, mut events: ActivityRecorderEvents) {
        let mut writer = DataWriter::<File>::new();
        let mut stats = Statistics::default();
        while let Ok(cmd) = receiver.recv() {
            // TODO use a macro to match and call all this functions to forward the cmd parameter; adding the timestamp to all functions should make this easier
            match cmd.action {
                Action::Start => {
                    stats.start();
                    writer.start();
                }
                Action::Stop => {
                    stats.stop();
                    writer.stop();
                }
                Action::Pause => {
                    stats.pause();
                    writer.pause();
                }
                Action::Proceed => {
                    stats.proceed();
                    writer.proceed();
                }
                Action::NextSection => {
                    stats.next_section(cmd.timestamp);
                    writer.next_section(cmd.timestamp);
                }
                Action::PositionUpdate(position_info) => {
                    stats.position_update(cmd.timestamp, position_info);
                    writer.position_update(cmd.timestamp, position_info);
                }
                Action::HeartRateUpdate(heart_rate) => {
                    stats.heart_rate_update(cmd.timestamp, heart_rate);
                    writer.heart_rate_update(cmd.timestamp, heart_rate);
                }
                Action::Tick => {
                    stats.tick(cmd.timestamp);
                    writer.tick(cmd.timestamp);
                }
                Action::SaveRecording => {
                    writer.save_recording();
                }
                Action::DiscardRecording => {
                    writer.discard_recording();
                }
            }

            stats
                .recording
                .when_changed(|&rec| events.recording.pin_mut().notify(rec));
            stats
                .section_count
                .when_changed(|&count| events.sectionCount.pin_mut().notify(count));
            stats
                .duration_in_section
                .when_changed(|&dur| events.durationInSection.pin_mut().notify(dur));
        }
    }

    pub fn shutdown(&mut self) {
        self.controller.sender = None;
    }

    fn send(&mut self, action: Action) {
        let _ = self.controller.sender.as_mut().map(|sender| {
            let timestamp = Utc::now().timestamp();
            sender.send(Command { timestamp, action })
        });
    }

    // TODO use a macro to create all this functions

    pub fn start(&mut self) {
        self.send(Action::Start);
    }

    pub fn stop(&mut self) {
        self.send(Action::Stop);
    }

    pub fn pause(&mut self) {
        self.send(Action::Pause);
    }

    pub fn proceed(&mut self) {
        self.send(Action::Proceed);
    }

    pub fn next_section(&mut self) {
        self.send(Action::NextSection);
    }

    pub fn position_update(&mut self, position_info: PositionInfo) {
        self.send(Action::PositionUpdate(position_info));
    }

    pub fn heart_rate_update(&mut self, heart_rate: u32) {
        self.send(Action::HeartRateUpdate(heart_rate));
    }

    pub fn tick(&mut self) {
        self.send(Action::Tick);
    }

    pub fn save_recording(&mut self) {
        self.send(Action::SaveRecording);
    }

    pub fn discard_recording(&mut self) {
        self.send(Action::DiscardRecording);
    }
}

pub fn new_activity_recorder_events() -> ActivityRecorderEvents {
    ActivityRecorderEvents {
        recording: ffi::new_event_bool(),
        sectionCount: ffi::new_event_u64(),
        durationInSection: ffi::new_event_u64(),
    }
}
unsafe impl Send for ActivityRecorderEvents {}

#[cxx::bridge(namespace = "kuri::rs")]
mod ffi {
    pub struct ActivityRecorder {
        controller: Box<ActivityRecorderController>,
    }

    pub struct ActivityRecorderEvents {
        pub recording: UniquePtr<EventBool>,
        pub sectionCount: UniquePtr<EventU64>,
        pub durationInSection: UniquePtr<EventU64>,
    }

    #[derive(Debug, Clone, Copy)]
    pub struct PositionInfo {
        pub timestamp: i64,
        pub latitude: f64,
        pub longitude: f64,
        pub altitude: f32,
        pub horizontal_accuracy: u16,
        pub vertical_accuracy: u16,
    }

    extern "Rust" {
        type ActivityRecorderController;

        fn new_activity_recorder_events() -> ActivityRecorderEvents;

        fn new_activity_recorder(events: ActivityRecorderEvents) -> ActivityRecorder;

        fn shutdown(self: &mut ActivityRecorder);

        // TODO check if a macro could be used to create all of this functions
        fn start(self: &mut ActivityRecorder);
        fn stop(self: &mut ActivityRecorder);
        fn pause(self: &mut ActivityRecorder);
        fn proceed(self: &mut ActivityRecorder);
        fn next_section(self: &mut ActivityRecorder);
        fn position_update(self: &mut ActivityRecorder, position_info: PositionInfo);
        fn heart_rate_update(self: &mut ActivityRecorder, heart_rate: u32);
        fn tick(self: &mut ActivityRecorder);
        fn save_recording(self: &mut ActivityRecorder);
        fn discard_recording(self: &mut ActivityRecorder);
    }

    #[namespace = "kuri::ffi"]
    unsafe extern "C++" {
        include!("kuri-rs/ffi/events.h");
        type EventBool;
        pub fn new_event_bool() -> UniquePtr<EventBool>;
        pub fn notify(self: Pin<&mut EventBool>, value: bool);

        type EventU64;
        pub fn new_event_u64() -> UniquePtr<EventU64>;
        pub fn notify(self: Pin<&mut EventU64>, value: u64);
    }
}
