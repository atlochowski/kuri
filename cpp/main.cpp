/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "activity_recorder.hpp"
#include "activityhistory.h"
#include "device.h"
#include "geo_position_info_source.hpp"
#include "light.h"
#include "pebblemanagercomm.h"
#include "pebblewatchcomm.h"
#include "settings.h"
#include "trackloader.h"
#include "trackrecorder.h"

#include <sailfishapp.h>

#include <QGuiApplication>
#include <QtGlobal>
#include <QtQml/QtQml>
#include <QtQuick/QQuickView>

QByteArray encryptDecryptKey(QByteArray key) {
    for (int i = 0; i < key.size() / 2; ++i) {
        const int  left  = 2 * i;
        const int  right = left + 1;
        const auto temp  = key.at(left);
        key[left]        = key[right];
        key[right]       = temp;
    }
    return key;
}

int main(int argc, char* argv[]) {
    // disable debug logging of category `default` (only qDebug from C++; not console.log) since QGridLayout spams the console
    QLoggingCategory::setFilterRules("default.debug=false\n");

    QScopedPointer<QGuiApplication> app {SailfishApp::application(argc, argv)};

    app->setApplicationName("Kuri");
    app->setApplicationVersion(QString(APP_VERSION));

    qDebug() << app->applicationName() << " version " << app->applicationVersion();

    qmlRegisterType<TrackLoader>("harbour.kuri", 1, 0, "TrackLoader");

    qmlRegisterSingletonType(SailfishApp::pathTo("qml/components/RecordPageColorTheme.qml"), "harbour.kuri", 1, 0, "RecordPageColorTheme");

    Settings              settings;
    Light                 light;
    ActivityHistory       history;
    GeoPositionInfoSource geoPositionInfo;
    ActivityRecorder      activityRecorder;
    TrackRecorder         trackRecorder {geoPositionInfo.updateInterval()};
    Device                hrmDevice;
    PebbleManagerComm     pebbleManagerComm;
    PebbleWatchComm       pebbleWatchComm;

    QObject::connect(&geoPositionInfo, &GeoPositionInfoSource::positionUpdated, &activityRecorder, &ActivityRecorder::positionUpdate);
    QObject::connect(&geoPositionInfo, &GeoPositionInfoSource::positionUpdated, &trackRecorder, &TrackRecorder::positionUpdated);

    QScopedPointer<QQuickView> view {SailfishApp::createView()};
    view->rootContext()->setContextProperty("appVersion", app->applicationVersion());
    view->rootContext()->setContextProperty("Settings", &settings);
    view->rootContext()->setContextProperty("Light", &light);
    view->rootContext()->setContextProperty("GeoPositionInfo", &geoPositionInfo);
    view->rootContext()->setContextProperty("ActivityHistory", &history);
    view->rootContext()->setContextProperty("ActivityRecorder", &activityRecorder);
    view->rootContext()->setContextProperty("TrackRecorder", &trackRecorder);
    view->rootContext()->setContextProperty("HrmDevice", &hrmDevice);
    view->rootContext()->setContextProperty("PebbleManagerComm", &pebbleManagerComm);
    view->rootContext()->setContextProperty("PebbleWatchComm", &pebbleWatchComm);

    // NOTE: Please don't reuse this strava client ID for your app if you copy this code but create a separate account
    // https://developers.strava.com/docs/getting-started/#account
    // https://developers.strava.com/docs/authentication/
    // https://developers.strava.com/docs/reference/
    view->rootContext()->setContextProperty("STRAVA_CLIENT_SECRET", encryptDecryptKey("55d77b7326d6e26ae64ef9e1ffe2e2cde20fd674"));
    view->rootContext()->setContextProperty("STRAVA_CLIENT_ID", "75580");

    view->setSource(SailfishApp::pathTo("qml/harbour-kuri.qml"));
    view->showFullScreen();

    return app->exec();
}
