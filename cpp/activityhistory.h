/*
 * Copyright (C) 2017 Jens Drescher, Germany
 * Copyright (C) 2021 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QAbstractListModel>
#include <QDateTime>
#include <QList>
#include <QSortFilterProxyModel>
#include <QtConcurrent>

struct TrackItem {
    QString   filename;
    QString   name;
    QString   workout;
    QDateTime time;
    int       duration {0};
    qreal     distance {0.};
    qreal     speed {0.};
    QString   stKey;
    QString   description;
    QString   fileSize;
    QString   fileLastModified;
};

class ActivityHistory : public QSortFilterProxyModel {
    Q_OBJECT

public:
    explicit ActivityHistory(QObject* parent = nullptr);
    ~ActivityHistory();

    Q_PROPERTY(int numberOfTracksToLoad READ numberOfTracksToLoad NOTIFY numberOfTracksToLoadChanged)
    Q_PROPERTY(int numberOfLoadedTracks READ numberOfLoadedTracks NOTIFY numberOfLoadedTracksChanged)
    Q_PROPERTY(bool varyingWorkoutTypes READ varyingWorkoutTypes NOTIFY varyingWorkoutTypesChanged)

    Q_INVOKABLE bool removeTrack(int row);

    Q_INVOKABLE void loadFile(QString fileName);
    Q_INVOKABLE void loadAllFiles();
    Q_INVOKABLE void saveAccelerationFile();
    Q_INVOKABLE void loadAccelerationFile();

    Q_INVOKABLE QString duration() const;
    Q_INVOKABLE qreal   distance();

    Q_INVOKABLE QString getSportsTrackerKey(const int sourceRow) const;

    Q_INVOKABLE QString   workouttypeAt(int row);
    Q_INVOKABLE int       durationAt(int row);
    Q_INVOKABLE qreal     distanceAt(int row);
    Q_INVOKABLE QDateTime dateAt(int row);

    Q_INVOKABLE int  numberOfTracksToLoad() const;
    Q_INVOKABLE int  numberOfLoadedTracks() const;
    Q_INVOKABLE bool varyingWorkoutTypes() const;

signals:
    void trackLoadingFinished();
    void numberOfTracksToLoadChanged();
    void numberOfLoadedTracksChanged();
    void varyingWorkoutTypesChanged();

public slots:
    void newTrackData(int num);
    void loadingFinished();

private slots:
    void handleModelDataChange(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles = QVector<int>());

private:
    class Model : public QAbstractListModel {
    public:
        explicit Model(QObject* parent = nullptr);

        enum HistoryRoles { FilenameRole = Qt::UserRole + 1, WorkoutRole, DateTimeRole, DurationRole, DistanceRole, SpeedRole, DescriptionRole };

        int      rowCount(const QModelIndex&) const override;
        QVariant data(const QModelIndex& index, int role) const override;
        QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

        bool          setData(const QModelIndex& index, const QVariant& value, int role) override;
        Qt::ItemFlags flags(const QModelIndex& index) const override;

        const QList<TrackItem>& tracks() const;

        void addTrack(const TrackItem track);
        void removeTrack(int row);

    protected:
        QHash<int, QByteArray> roleNames() const override;

    private:
        QList<TrackItem> m_trackList;
    };

private:
    void updateVaryingWorkoutTypes();
    void loadTracks();
    int  mapToSourceRow(int row);

private:
    Model                     m_model;
    QStringList               m_trackFilesToLoad;
    QFutureWatcher<TrackItem> m_trackLoading;
    int                       m_numberOfTracksToLoad {0};
    int                       m_workoutDuration {0};
    qreal                     m_workoutDistance {0.};
    bool                      m_gpxFilesChanged {true};
    bool                      m_varyingWorkoutTypes {false};
};
