#include "pebblemanagercomm.h"

#include <QDebug>

PebbleManagerComm::PebbleManagerComm(QObject* parent)
    : QObject(parent) {
    // Initialize DBus interface
    m_dbusPebbleManager = new QDBusInterface(SERVER_SERVICE, SERVER_PATH, SERVER_INTERFACE_MANAGER, QDBusConnection::sessionBus(), this);
}

PebbleManagerComm::~PebbleManagerComm() {
    m_dbusPebbleManager = nullptr;
}

QList<QString> PebbleManagerComm::getListWatches() {
    QList<QString> pebbleList;

    QDBusReply<QList<QDBusObjectPath>> reply = m_dbusPebbleManager->call("ListWatches");

    if (reply.isValid()) {
        QList<QDBusObjectPath> devices = reply.value();

        for (int i = 0; i < devices.count(); i++) {
            pebbleList.append(devices.at(i).path());
        }

        // pebbleList.append("Tester Gerät 2");
        // qDebug()<<"1 Pebble: " <<pebbleList.at(0);
    } else {
        qDebug() << "DBus error: " << reply.error().message();
    }

    return pebbleList;
}

QString PebbleManagerComm::getRockpoolVersion() {
    QDBusReply<QString> reply = m_dbusPebbleManager->call("Version");

    if (reply.isValid()) {
        return reply.value();
    } else {
        qDebug() << "DBus error: " << reply.error().message();
        return "";
    }
}
