#pragma once

#include <QObject>
#include <QtDBus/QtDBus>

#define SERVER_INTERFACE_MANAGER "org.rockwork.Manager"
#define SERVER_SERVICE "org.rockwork"
#define SERVER_PATH "/org/rockwork/Manager"

class PebbleManagerComm : public QObject {
    Q_OBJECT

public:
    PebbleManagerComm(QObject* parent = nullptr);
    ~PebbleManagerComm();

    Q_INVOKABLE QString getRockpoolVersion();
    Q_INVOKABLE QList<QString> getListWatches();

private:
    QDBusInterface* m_dbusPebbleManager {nullptr};
};
