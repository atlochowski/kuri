/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QGeoPositionInfoSource>
#include <QObject>
#include <QTimer>

class TrackRecorder : public QObject {
    Q_OBJECT
    Q_PROPERTY(int points READ points NOTIFY pointsChanged)
    Q_PROPERTY(qreal distance READ distance NOTIFY distanceChanged)
    Q_PROPERTY(qreal speed READ speed NOTIFY speedChanged)
    Q_PROPERTY(qreal pace READ pace NOTIFY paceChanged)
    Q_PROPERTY(QString paceStr READ paceStr NOTIFY paceChanged)
    Q_PROPERTY(QString paceImperialStr READ paceImperialStr NOTIFY paceChanged)
    Q_PROPERTY(qreal speedaverage READ speedaverage NOTIFY speedaverageChanged)
    Q_PROPERTY(qreal paceaverage READ paceaverage NOTIFY paceaverageChanged)
    Q_PROPERTY(qreal heartrateaverage READ heartrateaverage NOTIFY heartrateaverageChanged)
    Q_PROPERTY(qreal currentHeartRate READ currentHeartRate WRITE setCurrentHeartRate NOTIFY currentHeartRateChanged)
    Q_PROPERTY(QString paceaverageImperialStr READ paceaverageImperialStr NOTIFY paceaverageChanged)
    Q_PROPERTY(QString paceaverageStr READ paceaverageStr NOTIFY paceaverageChanged)
    Q_PROPERTY(QString time READ time NOTIFY timeChanged)
    Q_PROPERTY(QString pebbleTime READ pebbleTime NOTIFY pebbleTimeChanged)
    Q_PROPERTY(bool isEmpty READ isEmpty NOTIFY isEmptyChanged)
    Q_PROPERTY(QString workoutType READ workoutType WRITE setWorkoutType)
    Q_PROPERTY(QString startingDateTime READ startingDateTime)
    Q_PROPERTY(double altitude READ altitude NOTIFY valuesChanged)
    Q_PROPERTY(bool pause READ pause WRITE setPause NOTIFY pauseChanged)
    Q_PROPERTY(bool running READ running WRITE setRunning NOTIFY runningChanged)
    Q_PROPERTY(QString pauseTime READ pauseTime NOTIFY pauseTimeChanged)
    Q_PROPERTY(QString pebblePauseTime READ pebblePauseTime NOTIFY pauseTimeChanged)
    Q_PROPERTY(qint64 timeSeconds READ timeSeconds NOTIFY timeChanged)

public:
    explicit TrackRecorder(int updateInterval, QObject* parent = nullptr);
    ~TrackRecorder();
    Q_INVOKABLE bool    writeStGpxToFile(QString gpxcontent, qint64 recorded, QString desc, QString sTkey, QString activity, float dist);
    Q_INVOKABLE QString fileName();
    Q_INVOKABLE void    exportGpx(QString name = "", QString desc = "");
    Q_INVOKABLE void    clearTrack();

    int     points() const;
    qreal   distance() const;
    qreal   speed() const;
    qreal   pace() const;
    QString paceStr() const;
    QString paceImperialStr() const;
    qreal   speedaverage() const;
    qreal   paceaverage() const;
    uint    currentHeartRate() const;
    void    setCurrentHeartRate(uint heartRate);
    qreal   heartrateaverage() const;
    QString paceaverageStr() const;
    QString paceaverageImperialStr() const;
    QString time() const;
    QString pebbleTime() const;
    QString pebblePauseTime() const;
    QString pauseTime() const;
    bool    isEmpty() const;
    QString workoutType() const;
    void    setWorkoutType(QString workoutType);
    QString startingDateTime() const;
    double  altitude() const;
    bool    pause() const;
    bool    running() const;
    void    setPause(bool pause);
    void    setRunning(bool running);
    qint64  timeSeconds() const;

    Q_INVOKABLE QGeoCoordinate trackPointAt(int index);
    Q_INVOKABLE bool           pausePointAt(int index);

    // Temporary "hacks" to get around misbehaving Map.fitViewportToMapItems()
    Q_INVOKABLE int            fitZoomLevel(int width, int height);
    Q_INVOKABLE QGeoCoordinate trackCenter();

signals:
    void pointsChanged();
    void distanceChanged();
    void speedChanged();
    void paceChanged();
    void speedaverageChanged();
    void paceaverageChanged();
    void currentHeartRateChanged();
    void heartrateaverageChanged();
    void timeChanged();
    void pebbleTimeChanged();
    void isEmptyChanged();
    void valuesChanged();
    void newTrackPoint(QGeoCoordinate coordinate, int iPointIndex);
    void pauseChanged();
    void runningChanged();
    void pauseTimeChanged();

public slots:
    void positionUpdated(const QGeoPositionInfo& newPos);
    void autoSave();

private:
    void loadAutoSave();

private:
    QList<QGeoPositionInfo> m_points;
    QList<uint>             m_heartrate;
    QList<qreal>            m_distancearray;
    QList<bool>             m_pausearray;
    QGeoCoordinate          m_currentPosition;
    qreal                   m_distance {0.};
    qreal                   m_speed {0.};
    qreal                   m_pace {0.};
    qreal                   m_speedaverage {0.};
    qreal                   m_paceaverage {0.};
    qreal                   m_heartrateaverage {0.};
    uint                    m_heartrateadded {0u};
    qreal                   m_minLat {0.};
    qreal                   m_maxLat {0.};
    qreal                   m_minLon {0.};
    qreal                   m_maxLon {0.};
    bool                    m_isEmpty {true};
    int                     m_autoSavePosition {0};
    QTimer                  m_autoSaveTimer;
    uint                    m_currentHeartRate {0u};
    QString                 m_workoutType {"running"};
    double                  m_altitude {0.};
    bool                    m_pause {false};
    bool                    m_running {false};
    quint32                 m_PauseDuration {0u};
    int                     m_updateInterval {1000u};
};
