/*
 * Copyright (C) 2022 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "kuri-rs/ffi/events.h"
#include "kuri-rs/rust/activity_recorder.rs.h"

#include <QGeoPositionInfo>
#include <QObject>
#include <QTimer>

class ActivityRecorder : public QObject {
    Q_OBJECT

public:
    ActivityRecorder();
    ~ActivityRecorder();

    ActivityRecorder(const ActivityRecorder&) = delete;
    ActivityRecorder(ActivityRecorder&&)      = delete;

    ActivityRecorder& operator=(const ActivityRecorder&) = delete;
    ActivityRecorder& operator=(ActivityRecorder&&) = delete;

    void positionUpdate(const QGeoPositionInfo& update);
    // TODO this is currently called from QML because some parsing for non BTLE devices is done there;
    //      the parsing should be moved to C++/Rust, e.g. a HrmSource class
    Q_INVOKABLE void heartRateUpdate(const quint32 update);

    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();
    Q_INVOKABLE void pause();
    Q_INVOKABLE void proceed();
    Q_INVOKABLE void nextSection();
    Q_INVOKABLE void saveRecording();
    Q_INVOKABLE void discardRecording();

    Q_PROPERTY(bool recording READ recording NOTIFY recordingChanged)
    Q_PROPERTY(quint64 sectionCount READ sectionCount NOTIFY sectionCountChanged)
    Q_PROPERTY(quint64 durationInSection READ durationInSection NOTIFY durationInSectionChanged)
    Q_PROPERTY(QString durationInSectionAsMinutesSeconds READ durationInSectionAsMinutesSeconds NOTIFY durationInSectionChanged)

    bool     recording() const;
    uint64_t sectionCount() const;
    uint64_t durationInSection() const;
    QString  durationInSectionAsMinutesSeconds() const;

signals:
    void recordingChanged();
    void sectionCountChanged();
    void durationInSectionChanged();

private:
    void setRecording(bool value);
    void setSectionCount(uint64_t value);
    void setDurationInSection(uint64_t value);

    const QDateTime& epoche();
    uint16_t         millisecondsToNextTick() const;

    kuri::rs::ActivityRecorder initActivityRecorderRs();

private:
    static constexpr uint16_t INTERVAL_IN_MILLISECONDS {1000};
    static constexpr uint16_t OFFSET_IN_MILLISECONDS {200};

    kuri::rs::ActivityRecorder m_recorder {initActivityRecorderRs()};

    QTimer m_tickTimer;

    bool m_recordingStartPending {false};
    bool m_recording {false};

    uint64_t m_sectionCount {1};
    uint64_t m_durationInSection {0};
};
