#include "pebblewatchcomm.h"

#include <QDebug>

PebbleWatchComm::PebbleWatchComm(QObject* parent)
    : QObject(parent) {}

PebbleWatchComm::~PebbleWatchComm() {
    m_dbusPebble = nullptr;
}

void PebbleWatchComm::setServicePath(QString sServicePath) {
    m_dbusPebble = new QDBusInterface(SERVER_SERVICE, sServicePath, SERVER_INTERFACE_PEBBLE, QDBusConnection::sessionBus(), this);
}

QString PebbleWatchComm::getAddress() {
    if (m_dbusPebble == nullptr) { return ""; }

    QDBusReply<QString> reply = m_dbusPebble->call("Address");

    if (reply.isValid()) {
        return reply.value();
    } else {
        qDebug() << "DBus error: " << reply.error().message();
        return "";
    }
}

QString PebbleWatchComm::getName() {
    if (m_dbusPebble == nullptr) { return ""; }

    QDBusReply<QString> reply = m_dbusPebble->call("Name");

    if (reply.isValid()) {
        return reply.value();
    } else {
        qDebug() << "DBus error: " << reply.error().message();
        return "";
    }
}

bool PebbleWatchComm::isConnected() {
    if (m_dbusPebble == nullptr) { return false; }

    QDBusReply<bool> reply = m_dbusPebble->call("IsConnected");

    if (reply.isValid()) {
        return reply.value();
    } else {
        qDebug() << "DBus error: " << reply.error().message();
        return false;
    }
}
