/*
 * Copyright (C) 2022 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "activity_recorder.hpp"

#include "formatter.h"

#include <QDateTime>
#include <QDebug>

ActivityRecorder::ActivityRecorder()
    : QObject(nullptr) {
    m_tickTimer.setSingleShot(false);

    connect(&m_tickTimer, &QTimer::timeout, [&] {
        auto millisecondsToNextTick = this->millisecondsToNextTick();
        m_recorder.tick();
        // prevent double activation due to jitter in the wake-up
        if (millisecondsToNextTick < OFFSET_IN_MILLISECONDS) { millisecondsToNextTick += INTERVAL_IN_MILLISECONDS; };
        m_tickTimer.start(millisecondsToNextTick);
    });
}

const QDateTime& ActivityRecorder::epoche() {
    static const auto epoche = QDateTime::fromMSecsSinceEpoch(0);
    return epoche;
}

uint16_t ActivityRecorder::millisecondsToNextTick() const {
    auto currentMilliseconds = static_cast<uint16_t>(QDateTime::currentDateTimeUtc().time().msec());

    return (currentMilliseconds <= OFFSET_IN_MILLISECONDS) ? OFFSET_IN_MILLISECONDS - currentMilliseconds
                                                           : (INTERVAL_IN_MILLISECONDS + OFFSET_IN_MILLISECONDS) - currentMilliseconds;
}

kuri::rs::ActivityRecorder ActivityRecorder::initActivityRecorderRs() {
    auto events = kuri::rs::new_activity_recorder_events();
    connect(events.recording.get(), &kuri::ffi::EventBool::signal, this, &ActivityRecorder::setRecording, Qt::QueuedConnection);
    connect(events.sectionCount.get(), &kuri::ffi::EventU64::signal, this, &ActivityRecorder::setSectionCount, Qt::QueuedConnection);
    connect(events.durationInSection.get(), &kuri::ffi::EventU64::signal, this, &ActivityRecorder::setDurationInSection, Qt::QueuedConnection);
    return kuri::rs::new_activity_recorder(std::move(events));
}

ActivityRecorder::~ActivityRecorder() {
    m_recorder.shutdown();
}

void ActivityRecorder::positionUpdate(const QGeoPositionInfo& update) {
    // the position is updated every second; if there is no position update for more than a second, let the tickTimer trigger a timeout
    if (m_recordingStartPending || m_recording) { m_tickTimer.start(INTERVAL_IN_MILLISECONDS + OFFSET_IN_MILLISECONDS); }

    auto timestamp         = update.timestamp().toUTC();
    auto secondsSinceEpoch = epoche().secsTo(timestamp);

    kuri::rs::PositionInfo positionInfo;
    positionInfo.timestamp = secondsSinceEpoch;
    auto coordinate        = update.coordinate();
    positionInfo.latitude  = coordinate.latitude();
    positionInfo.longitude = coordinate.longitude();
    positionInfo.altitude  = static_cast<float>(coordinate.altitude());
    if (update.hasAttribute(QGeoPositionInfo::HorizontalAccuracy)) {
        positionInfo.horizontal_accuracy = static_cast<uint16_t>(update.attribute(QGeoPositionInfo::HorizontalAccuracy));
    } else {
        positionInfo.horizontal_accuracy = 0;
    }
    if (update.hasAttribute(QGeoPositionInfo::VerticalAccuracy)) {
        positionInfo.vertical_accuracy = static_cast<uint16_t>(update.attribute(QGeoPositionInfo::VerticalAccuracy));
    } else {
        positionInfo.vertical_accuracy = 0;
    }
    m_recorder.position_update(positionInfo);
}

void ActivityRecorder::heartRateUpdate(const quint32 update) {
    m_recorder.heart_rate_update(update);
}

void ActivityRecorder::start() {
    m_recordingStartPending = true;
    // even if there is less than OFFSET_IN_MILLISECONDS time to the next tick,
    // either there are already position data available and those will be used
    // or there are none and most probably none will arrive withing the next second
    auto millisecondsToNextTick = this->millisecondsToNextTick();
    m_recorder.start();
    m_tickTimer.start(millisecondsToNextTick);
}

void ActivityRecorder::stop() {
    m_recordingStartPending = false;
    m_tickTimer.stop();
    m_recorder.stop();
}

void ActivityRecorder::pause() {
    m_recorder.pause();
}

void ActivityRecorder::proceed() {
    m_recorder.proceed();
}

void ActivityRecorder::nextSection() {
    m_recorder.next_section();
}

void ActivityRecorder::saveRecording() {
    m_recorder.save_recording();
}

void ActivityRecorder::discardRecording() {
    m_recorder.discard_recording();
}

bool ActivityRecorder::recording() const {
    return m_recording;
}

void ActivityRecorder::setRecording(bool value) {
    m_recordingStartPending = false;
    m_recording             = value;
    emit recordingChanged();
}

uint64_t ActivityRecorder::sectionCount() const {
    return m_sectionCount;
}

void ActivityRecorder::setSectionCount(uint64_t value) {
    m_sectionCount = value;
    emit sectionCountChanged();
}

uint64_t ActivityRecorder::durationInSection() const {
    return m_durationInSection;
}

QString ActivityRecorder::durationInSectionAsMinutesSeconds() const {
    return Formatter::secondsToMinutesSeconds(m_durationInSection);
}

void ActivityRecorder::setDurationInSection(uint64_t value) {
    m_durationInSection = value;
    emit durationInSectionChanged();
}
