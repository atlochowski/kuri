// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use std::fs::File;
use std::io::{BufRead, BufReader};

use anyhow::Result;

fn main() -> Result<()> {
    let rust_cxx_bridge_file_list = "rust_cxx_bridge_file_list.txt";
    println!("cargo:rerun-if-changed={}", rust_cxx_bridge_file_list);

    let file = File::open(rust_cxx_bridge_file_list)?;
    let reader = BufReader::new(file);

    let bridge_files: Vec<String> = reader
        .lines()
        .flatten()
        .map(|line| line.trim().to_string())
        .filter(|line| line.len() > 0)
        .collect();

    // ignore the cross compile check for pkg-config; this is safe since it is only used for cxx to check signatures
    std::env::set_var("PKG_CONFIG_ALLOW_CROSS", "1");
    let qt_lib = pkg_config::Config::new()
        .atleast_version("5.6")
        .probe("Qt5Core")?;

    cxx_build::bridges(&bridge_files)
        .includes(qt_lib.include_paths)
        .flag_if_supported("-std=c++17")
        .compile("kuri-rs");

    for bridge_file in bridge_files.iter() {
        println!("cargo:rerun-if-changed={}", bridge_file);
    }

    Ok(())
}
