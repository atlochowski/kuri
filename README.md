# Kuri

Kuri is a sport tracking application for Sailfish OS. Kuri means "run" in Esperanto. Funnily it also means "discipline" in Finnish.
Features are:
- recording tracks
- voice coach
- view recorded track on a map and show statistics
- save track as GPX file
- connecting to bluetooth heart rate device
- uploading to Strava and Sport-Tracker.com
- Pebble support (needs Rockpool)
- and more...

This application originates from a fork of Laufhelden by Jens Drescher: https://github.com/jdrescher2006/Laufhelden

# Credits

- Jens Dresher and all the contributors of Laufhelden

Workout icons are from here https://icons8.com (license: https://creativecommons.org/licenses/by-nd/3.0/)

# License

License: GNU General Public License (GNU GPLv3)

# Icon Color Palette

- `#c02222` red
- `#c06622` orange
- `#c0a622` yellow
- `#22c071` green
- `#228bc0` blue
- `#7e22c0` violet
- `#c022c0` purple
- `#707b80` silver
- `#333333` pebble

# Development

With the introduction of Rust, the Docker based SDK is required to build Kuri.

## Basics

`sfdk` is used to build Kuri, see also [sfdk](https://sailfishos.org/wiki/Tutorial_-_Building_packages_-_advanced_techniques)

- create alias for `sfdk`
  - `alias sfdk=~/SailfishOS/bin/sfdk`
- list targets
  - `sfdk tools list`
- set target from within build directory
  - `sfdk config target=SailfishOS-4.3.0.12-i486`
- build
  - `sfdk build -j 4 ..`
  - rpm is in `RPM` folder
- check for compatibility with store
  - `sfdk check RPMS/harbour-kuri.rpm`
- list devices
  - `sfdk device list`
- set emulator
  - `sfdk config device="Sailfish OS Emulator 4.3.0.12"`
- deploy to emulator
  - `sfdk deploy --sdk`
- run in emulator
  - `sfdk device exec harbour-kuri`
- enter emulator environment
  - `sfdk device exec`
- access build environment
  - `sfdk build-shell ls`
- enter build environment
  - `sfdk build-shell`

## Emulator Setup

In order to run Kuri in the emulator, `mapbox-gl.rpm` must be installed. The easiest way to do this is by the `chum` repository.
Get the [chum rpm](https://chumrpm.netlify.app/) download link for the specific emulator, e.g.
`https://repo.sailfishos.org/obs/sailfishos:/chum//4.3.0.12_i486//noarch/sailfishos-chum-0.3.0-1.5.1.jolla.noarch.rpm`.

Instructions to setup emulator:

```console
alias sfdk=~/SailfishOS/bin/sfdk
sfdk config target=SailfishOS-4.3.0.12-i486
sfdk config device="Sailfish OS Emulator 4.3.0.12"
sfdk device exec curl -O https://repo.sailfishos.org/obs/sailfishos:/chum//4.3.0.12_i486//noarch/sailfishos-chum-0.3.0-1.5.1.jolla.noarch.rpm
sfdk device exec sudo pkcon install-local -y sailfishos-chum-0.3.0-1.5.1.jolla.noarch.rpm
sfdk device exec sudo pkcon refresh
```

To make Strava work in the emulator, a browser needs to be installed, e.g. by `sudo pkcon install sailfish-browser`.

## Build and Test

Instructions to build Kuri:

```console
$ mkdir -p build/i486
$ cd build/i486
$ alias sfdk=~/SailfishOS/bin/sfdk
$ sfdk config target=SailfishOS-4.3.0.12-i486
$ sfdk build -j 4 ..
$ sfdk check RPMS/harbour-kuri.rpm
```

Instructions to run Kuri on the emulator:

```console
$ sfdk config device="Sailfish OS Emulator 4.3.0.12"
$ sfdk deploy --sdk
$ sfdk device exec harbour-kuri
```
